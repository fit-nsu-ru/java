package school.lesson3.classes;

public class ConstructorDemo {

   public static void main(String[] args) {

      ConstructorEmployee[] staff = new ConstructorEmployee[3];

      staff[0] = new ConstructorEmployee("Carl Cracker", 55000);
      staff[1] = new ConstructorEmployee("Harry Hacker", 40000);
      staff[2] = new ConstructorEmployee("Tony Tester", 25000);

      for (ConstructorEmployee e : staff)
         e.raiseSalary(5);

      for (ConstructorEmployee e : staff)
         System.out.println("name=" + e.getName() + ",salary=" + e.getSalary());
   }
}

class ConstructorEmployee {

   public ConstructorEmployee(String name, double salary) {
      this.name = name;
      this.salary = salary;
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public void raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
   }

   private String name;
   private double salary;
}


