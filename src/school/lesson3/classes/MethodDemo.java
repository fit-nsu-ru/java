package school.lesson3.classes;

public class MethodDemo {
   
   public static void main(String[] args) {

      String[] names = { "Carl Cracker", "Harry Hacker", "Tony Tester" };
      MethodEmployee[] staff = new MethodEmployee[3];

      for (int i = 0; i < 3; i++) {

         staff[i] = new MethodEmployee();
         staff[i].setName(names[i]);
         staff[i].setId();
         
      }

      for (MethodEmployee e : staff)
         System.out.println("name=" + e.getName() + ",id=" + e.getId());
   }
}

class MethodEmployee {

   public void setName(String n) {
      name = n;
   }

   public String getName() {
      return name;
   }

   public void setId() {
      id = nextId;
      nextId++;
   }

   public int getId() {
      return id;
   }

   private String name;
   private int id;
   private static int nextId = 1;
}
