package school.lesson3.classes;

public class FieldDemo {

   public static void main(String[] args) {

      String[] names = { "Carl Cracker", "Harry Hacker", "Tony Tester" };
      FieldEmployee[] staff = new FieldEmployee[3];
          
      for (int i = 0; i < 3; i++) {

         staff[i] = new FieldEmployee();
         staff[i].name = names[i];
         staff[i].id = FieldEmployee.nextId++;
      }
     
      for (FieldEmployee e : staff)
         System.out.println("name=" + e.name + ",id=" + e.id);
   }
}

class FieldEmployee {

   public int id;
   public String name;
   public static int nextId = 1;
}
