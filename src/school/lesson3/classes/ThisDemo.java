package school.lesson3.classes;

public class ThisDemo {

   public static void main(String[] args) {

      FluentEmployee e = new FluentEmployee("Tony Tester", 20000);

         e.raiseSalary(10).raiseSalary(10).raiseSalary(10);
         System.out.println("name=" + e.getName() + ",salary=" + e.getSalary());
   }
}

class FluentEmployee {

   public FluentEmployee(String name, double salary) {
      this.name = name;
      this.salary = salary;
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public FluentEmployee raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
      return this;
   }

   private String name;
   private double salary;
}


