package school.lesson3.inheritance;

class NamePrinter {

   public static void printNames(Person[] people) {

      for (Person person : people) {
         System.out.println(person.getName());
      }
   }
}

public class NamePrinterDemo {

   public static void main(String[] args) {

      Person[] people = new Person[3];

      people[0] = new Person("John Whatson");
      people[1] = new Employee("Harry Hacker", 25000);
      people[2] = new Manager("Carl Cracker", 30000.00, 7);

      System.out.println();
      NamePrinter.printNames(people);
   }
}
