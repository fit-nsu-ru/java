package school.lesson3.inheritance;

public class OverridingDemo {

   public static void main(String[] args) {

      Person person = new Person();
      person.setName("Robert");
      System.out.println(person.getDescription());
      
      Employee employee = new Employee();
      employee.setName("Robert");
      employee.setSalary(30000);
      System.out.println(employee.getDescription());
      
      Manager manager = new Manager();
      manager.setName("Robert");
      manager.setSalary(30000);
      manager.setReports(5);
      System.out.println(manager.getDescription());
   }
}
