package school.lesson3.inheritance;

public class InstanceOfDemo {

   public static void main(String[] args) {
      
      Manager manager = new Manager("Robert", 30000, 5);
     
      System.out.println("\nA manager: name = " + manager.getName()
            + ", salary = " + manager.getSalary() + ", reports = "
            + manager.getReports());

      System.out.println();
      
      System.out.println("Am I a manager: "
            + (manager instanceof Manager));
      
      System.out.println("Am I an employee: "
            + (manager instanceof Employee));
      
      System.out.println("Am I a person: "
            + (manager instanceof Person));
   }
}
