package school.lesson3.inheritance;

public class ConstructorDemo {

   public static void main(String[] args) {

      Manager manager = new Manager("Robert", 30000, 5);
      
      System.out.println();
      System.out.println("A manager: name = " + manager.getName()
            + ", salary = " + manager.getSalary() + ", direct reports = "
            + manager.getReports());
   }
}
