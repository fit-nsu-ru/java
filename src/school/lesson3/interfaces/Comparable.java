package school.lesson3.interfaces;

public interface Comparable {
   
   public int compareTo(Comparable other); 
   
}
