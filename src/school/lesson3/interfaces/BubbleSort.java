package school.lesson3.interfaces;

public class BubbleSort {

   public static void sort(Comparable[] toSort) {
          
      Comparable temp;

      for (int i = 1; i < toSort.length; i++) {
         for (int j = 0; j < toSort.length - i; j++) {

            if (toSort[j].compareTo(toSort[j + 1]) > 0) {
               temp = toSort[j + 1];
               toSort[j + 1] = toSort[j];
               toSort[j] = temp;
            }
         }
      }
   }
}
