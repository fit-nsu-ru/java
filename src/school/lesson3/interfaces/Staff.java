package school.lesson3.interfaces;

public class Staff {

   public static String getStatement(Employee[] emps) {
      
      StringBuilder sb = new StringBuilder("----------Statement----------\n");
      double totalPay = 0; 
      
      for(Employee emp: emps) {
      
         totalPay += emp.getSalary();
         sb.append(emp.getName()).append(" ").append(emp.getSalary()).append("\n");         
      }
      
      sb.append("----------------------------\n");
      sb.append("Total pay: ").append(totalPay);
      return sb.toString();
   }
}
