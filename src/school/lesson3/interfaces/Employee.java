package school.lesson3.interfaces;

public class Employee {

   public Employee(String name, double salary) {
      this.name = name;
      this.salary = salary;
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public Employee raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
      return this;
   }

   public String toString() {
      return "Employee [name=" + getName() + ", salary=" + getSalary() + "]";
   }

   private String name;
   private double salary;
}