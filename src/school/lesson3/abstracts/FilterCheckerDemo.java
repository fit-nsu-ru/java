package school.lesson3.abstracts;

public class FilterCheckerDemo {

   public static void main(String[] args) {

      Filter subs = new SubStringFilter("spam");
      Filter maxl = new MaxLengthFilter(15);
      Filter regex = new RegexFilter("\\d{7}");
      Filter minl = new MinLengthFilter(5);
      Filter prefix = new PrefixFilter("aaaaa");
      
      subs.setNext(maxl);
      maxl.setNext(regex);
      regex.setNext(minl);
      minl.setNext(prefix);
            
      String[] emails = {"long long long text","hello spam ","some text","abcd","aaaaabbbbb","1234567"};
      
      for(String email : emails) {
         FilterChecker.check(email, subs);
      }
   }
}
