package school.lesson3.abstracts;

public class VetDemo {

   public static void main(String[] args) {

      Animal[] zoo = new Animal[6];
      
      zoo[0] = new Cat("Amber");
      zoo[1] = new Dog("Jake");
      zoo[2] = new Cow("Daisy");
      zoo[3] = new Donkey("Sam");
      zoo[4] = new Lion("Max");
      zoo[5] = new BigDog("Baxter");
            
      Vet veterinarian = new Vet("John Whatson");
      veterinarian.makeShots(zoo);
   }
}
