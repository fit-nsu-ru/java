package school.lesson3.abstracts;

public abstract class Animal {

   public Animal(String name) {
      this.name = name;
   }
   
   public String getName() {
      return name;
   }
   
   public abstract String makeNoise();
   
   private String name;
}




class Cat extends Animal {
   
   public Cat(String name) {
      super(name);
   }
   
   public String makeNoise(){
      return "Meow";
   }
}

class Dog extends Animal {
   
   public Dog(String name) {
      super(name);
   }
   
   public String makeNoise(){
      return "Woof-woof";
   }
}

class BigDog extends Dog {
   
   public BigDog(String name) { 
      super(name);
   }
   
   public String makeNoise(){
      
      return super.makeNoise() + " "+ super.makeNoise();
   }
   
}


class Cow extends Animal {
   
   public Cow(String name) {
      super(name);
   }
      
   public String makeNoise(){
      return "Mooo";
   }
}

class Donkey extends Animal {
   
   public Donkey(String name) {
      super(name);
   }
      
   public String makeNoise(){
      return "Hee-haw";
   }
}

class Lion extends Animal {
   
   public Lion(String name) {
      super(name);
   }
   
   public String makeNoise(){
      return "Rawr";
   }
}

