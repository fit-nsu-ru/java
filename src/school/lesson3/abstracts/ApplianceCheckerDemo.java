package school.lesson3.abstracts;

public class ApplianceCheckerDemo {

   public static void main(String[] args) {

      Appliance[] apps = new Appliance[5];

      apps[0] = new Blender(200, 200);
      apps[1] = new Coffeemaker(500, 9);
      apps[2] = new Microwave(400, true);
      apps[3] = new Toaster(100, 2);
      apps[4] = new Refrigerator(800, "YELLOW");
      
      for (Appliance app : apps) {
         app.turnOn();
      }
      
      ApplianceChecker.checkAppliances(apps,1000);
   }
}
