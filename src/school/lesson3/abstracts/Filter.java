package school.lesson3.abstracts;

public abstract class Filter {
   
   private Filter next;
   
   public abstract boolean isOk(String email);
   
   public void setNext(Filter next) {
      this.next = next;
   }
   
   public Filter getNext() {
      return next;
   }
}

class MaxLengthFilter extends Filter {
   
   public MaxLengthFilter(int length) {
      this.length = length;      
   }

   public boolean isOk(String email) {

      if (email.length() > length) {

         System.out.println("Mail to long!");
         return false;
      }
      return true;
   }
   private int length;
}

class MinLengthFilter extends Filter {
   
   public MinLengthFilter(int length) {
      this.length = length;      
   }

   public boolean isOk(String email) {

      if (email.length() < length) {

         System.out.println("Mail to short!");
         return false;
      }
      return true;
   }
   private int length;
} 

class SubStringFilter extends Filter {
   
   public SubStringFilter(String keyword) {
      this.keyword = keyword;      
   }

   public boolean isOk(String email) {

      if (email.indexOf(keyword) >= 0) {

         System.out.println("Mail contains malicious text: " + keyword + "!");
         return false;
      }
      return true;
   }
   private String keyword;
} 


class RegexFilter extends Filter {
   
   public RegexFilter(String regex) {
      this.regex = regex;      
   }

   public boolean isOk(String email) {

      if (email.matches(regex)) {

         System.out.println("Mail contains malicious pattern: " + regex + "!");
         return false;
      }
      return true;
   }
   private String regex;
} 

class PrefixFilter extends Filter {
   
   public PrefixFilter(String prefix) {
      this.prefix = prefix;      
   }

   public boolean isOk(String email) {

      if (email.startsWith(prefix)) {
         System.out.println("Mail contains wrong prefix: " + prefix + "!");
         return false;
      }
      return true;
   }
   private String prefix;
}
