package school.lesson3.abstracts;

public abstract class Appliance {

   public Appliance(int power) {
      this.power = power;
   }

   public int getPower() {
      return power;
   }

   public boolean isOn() {
      return on;
   }

   public void turnOff() {
      on = false;
   }
   
   public void turnOn() {
      on = true;
   }

   public abstract String getDescription();

   private int power;
   private boolean on;
}

class Blender extends Appliance {

   public Blender(int power, int rpms) {
      super(power);
      this.rpms = rpms;
   }

   public String getDescription() {
      return "a " + rpms + " rpm blender";
   }
   
   private int rpms;
}

class Coffeemaker extends Appliance{

   public Coffeemaker(int power,  int cups) {
      super(power);
      this.cups = cups;
   }
   
   public String getDescription() {
      return "a " + cups + " cup coffee maker";
   }
     
   private int cups;
}

class Refrigerator extends Appliance{

   public Refrigerator(int power, String color) {
      super(power);
      this.color = color;
   }
   
   public String getDescription() {
      return "a "+ color + " refrigerator";
   }
     
   private String color;
}

class Microwave extends Appliance{

   public Microwave(int power, boolean convection) {
      super(power);
      this.convection = convection;
   }
   
   public String getDescription() {
      return "a "+ (convection? "convection":"") + " microwave";
   }
     
   private boolean convection;
}

class Toaster extends Appliance{

   public Toaster(int power, int slices) {
      super(power);
      this.slices = slices;
   }
   
   public String getDescription() {
      return "a "+ slices + " slice toaster";
   }
     
   private int slices;
}


