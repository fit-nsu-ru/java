package school.lesson3.obectmeth;

public class EqualsDemo {
   public static void main(String[] args) {

      System.out.println("Creating Employee object and initializing alice1 reference variable ...");
      Employee alice1 = new Employee("Alice Coder", 25000, 1987, 12, 15);
      System.out.println("alice1: " + alice1);
      
      System.out.println("Copying reference from alice1 to alice2 ...");
      Employee alice2 = alice1;

      System.out.println("alice1 and alice2 reference the same object: " + (alice1 == alice2));
      System.out.println("alice1 and alice2 reference equal objects: " + alice1.equals(alice2) + "\n");
            
      System.out.println("Creating Employee object and initializing alice3 reference variable ...");
      Employee alice3 = new Employee("Alice Coder", 25000, 1987, 12, 15);
      System.out.println("alice1: " + alice3);
      
      System.out.println("alice1 and alice3 reference the same object: " + (alice1 == alice3));
      System.out.println("alice1 and alice3 reference equal objects: " + alice1.equals(alice3) + "\n");
   
      System.out.println("Creating Employee object and initializing bob reference variable ...");
      Employee bob = new Employee("Bob Proger", 25000, 1987, 12, 15);
      System.out.println("bob: " + bob);
      
      System.out.println("alice1 and bob reference the same object: " + (alice1 == bob));
      System.out.println("alice1 and bob reference equal objects: " + alice1.equals(bob));
   }
}
