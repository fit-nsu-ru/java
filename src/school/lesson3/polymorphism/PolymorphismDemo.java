package school.lesson3.polymorphism;

public class PolymorphismDemo {
   
   public static void main(String[] args) {

 //     Person[] people = new Person[3];
      Person[] people = new Person[4];

      
      people[0] = new Person("John Whatson");
      people[1] = new Employee("Harry Hacker", 25000);
      people[2] = new Manager("Carl Cracker", 30000.00, 7);
      people[3] = new Student("Tonny Tester", "Applied mathematics");

      System.out.println();
      DescriptionPrinter.printDescriptions(people);
   }
}
