package school.lesson3.polymorphism;

public class SimplePolymorphismDemo {

   public static void main(String[] args) {

      Manager manager = new Manager("Robert",30000,5);
      
      System.out.println(manager.getDescription());

      Employee employee = manager;
      System.out.println(employee.getDescription());
      
      
      Person person = employee;
      System.out.println(person.getDescription());

   }
}
