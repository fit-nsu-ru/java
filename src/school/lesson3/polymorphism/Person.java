package school.lesson3.polymorphism;

class Person {
  
   public Person(String name){
      this.name = name;
   }
   
   public void setName(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public String getDescription() {
      return "A person: name = " + getName();
   }
   
   private String name;
}