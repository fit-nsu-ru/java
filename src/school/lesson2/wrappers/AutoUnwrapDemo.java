package school.lesson2.wrappers;

public class AutoUnwrapDemo {

   public static void main(String[] args) {

      Boolean wboo = new Boolean(false);
      boolean boo  = wboo;
      System.out.println("boolean variable's value: " + boo);
      
      Byte wbyte = new Byte((byte)2);
      byte b = wbyte;
      System.out.println("byte variable's value: " + b);

      Short wshort = new Short((short)4);
      short s = wshort;
      System.out.println("short variable's value: " + s);
      
      Integer wint = new Integer(16);
      int i = wint;
      System.out.println("int variable's value: " + i);

      Long wlong = new Long(123);
      long l = wlong;
      System.out.println("long variable's value: " + l);

      Float wfloat = new Float(12.34);
      float f = wfloat;
      System.out.println("float variable's value: " + f);

      Double wdouble = new Double(12.56);
      double d = wdouble;
      System.out.println("double variable's value: " + d);
   }
}
