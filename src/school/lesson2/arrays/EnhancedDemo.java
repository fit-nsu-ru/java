package school.lesson2.arrays;

public class EnhancedDemo {

   public static void main(String[] args) {

      int[] anArray = {0, 100, 200, 300, 400, 500, 600, 700, 800, 900 };

      for (int a : anArray) {

         System.out.println(a);
      }
   }
}
