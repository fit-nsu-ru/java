package school.lesson2.arrays;

import java.util.Arrays;

public class PrintDemo {

   public static void main(String[] args) {

      int[] anArray = { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };

      System.out.println(anArray.getClass().getName() + "@" + Integer.toHexString(anArray.hashCode()));
      System.out.println(anArray);
      System.out.println("ToString from Arrays: \n" + Arrays.toString(anArray));
   }
}
