package school.lesson2.arrays;

public class RefDemo {

   public static void main(String[] args) {

      String[] b = { "Apple", "Mango", "Orange" };
      System.out.println("Before Function Call: " + b[0]);
      RefDemo.changeElement(b);
      System.out.println("After Function Call: " + b[0]);

   }

   public static void changeElement(String[] a) {
      a[0] = "Changed";
   }
}
