package school.lesson2.primitives;

public class ByteIntConversionDemo {

   public static void main(String[] args) {

      int xInt;
      byte xByte;

      System.out.println("Implicit conversion byte to int ...");

      xByte = 100;
      xInt = xByte;

      System.out.println("xByte variable's value: " + xByte);
      System.out.println("xInt variable's value: " + xInt);

      System.out.println("\nNow explicit conversion int to byte ...");

      xInt = 150;
      xByte = (byte) xInt;

      System.out.println("xInt variable's value: " + xInt);
      System.out.println("xByte variable's value: " + xByte);
   }
}
