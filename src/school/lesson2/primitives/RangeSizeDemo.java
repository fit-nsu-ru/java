package school.lesson2.primitives;

public class RangeSizeDemo {

   public static void main(String[] args) {

      System.out.println("byte min: " + Byte.MIN_VALUE);
      System.out.println("byte max: " + Byte.MAX_VALUE);
      System.out.println("byte length: " + Byte.SIZE);

      System.out.println("short min: " + Short.MIN_VALUE);
      System.out.println("short max: " + Short.MAX_VALUE);
      System.out.println("short length: " + Short.SIZE);

      System.out.println("char min: " + (int) Character.MIN_VALUE);
      System.out.println("char max: " + (int) Character.MAX_VALUE);
      System.out.println("char length: " + Character.SIZE);

      System.out.println("int min: " + Integer.MIN_VALUE);
      System.out.println("int max: " + Integer.MAX_VALUE);
      System.out.println("int length: " + Integer.SIZE);

      System.out.println("long min: " + Long.MIN_VALUE);
      System.out.println("long max: " + Long.MAX_VALUE);
      System.out.println("long length: " + Long.SIZE);

      System.out.println("float min approx: " + Float.MIN_VALUE);
      System.out.println("float max approx: " + Float.MAX_VALUE);
      System.out.println("float length: " + Float.SIZE);

      System.out.println("double min approx: " + Double.MIN_VALUE);
      System.out.println("double max approx: " + Double.MAX_VALUE);
      System.out.println("double length: " + Double.SIZE);
   }
}
