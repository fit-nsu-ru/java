package school.lesson2.primitives;

public class ArithmeticExceptionDemo {

   public static void main(String[] args) {

      int a = 10;
      int b = 0;

      System.out.println("We will try to divide " + a + " by " + b);
      System.out.println(a / b);

   }
}
