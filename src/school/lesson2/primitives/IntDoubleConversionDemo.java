package school.lesson2.primitives;

public class IntDoubleConversionDemo {

   public static void main(String[] args) {

      int xInt;
      double xDouble;

      System.out.println("Implicit conversion int to double ...");

      xInt = 120;
      xDouble = xInt;

      System.out.println("xInt variable's value: " + xInt);
      System.out.println("xDouble variable's value: " + xDouble);

      System.out.println("\nNow explicit conversion double to int ...");

      xDouble = 3.8547;
      xInt = (int) xDouble;

      System.out.println("xDouble variable's value: " + xDouble);
      System.out.println("xInt variable's value: " + xInt);

   }
}
