package school.lesson2.primitives;

public class ByteDemo {

   public static void main(String[] args) {

      byte a = 10;
      byte b = 1;
      
      byte c = (byte)(a+b) ;
      
      System.out.println("And the result is: " + c);
   }
}
