package school.lesson2.reference;

public class SwapDemo {
   
   public static void main(String[] args) {
      
      Employee alice = new Employee("Alice", 20000);
      Employee bob = new Employee("Robert", 30000);
     
      System.out.println("Employee Alice: name = " + alice.name + ", salary = " + alice.salary);
  
      System.out.println("Employee Bob: name = " + bob.name + ", salary = " + bob.salary);
               
      Employee temp = alice;
      alice = bob;
      bob = temp;
      
      System.out.println("Employee Alice: name = " + alice.name + ", salary = " + alice.salary);
      System.out.println("Employee Bob: name = " + bob.name + ", salary = " + bob.salary);
   }
}
