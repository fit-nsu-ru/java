package school.lesson2.control;

public class BreakDemo {

   public static void main(String[] args) {

      String[] people = { "Tom", "Alice", "Bob", "John", "Harry", "Don", "Tony", "Carol" };

      for (int i = 0; i < people.length; i++) {

         System.out.println("Checking next person: " + people[i]);

         if (people[i].equals("Don")) {
            System.out.println("Found criminal Don");
            break;
         }
         if (people[i].equals("John")) {
            System.out.println("Found criminal John");
            break;
         }
      }
   }
}
