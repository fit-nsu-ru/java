package school.lesson2.control;

import java.util.Scanner;

public class TernaryDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      System.out.println("Enter your sales:");
      double yourSales = in.nextDouble();

      System.out.println("Enter your target:");
      double target = in.nextDouble();

      in.close();
      
      String performance = yourSales >= target ? "Satisfactory"
            : "Unsatisfactory";
      double bonus = yourSales >= target ? 10 + 0.05 * (yourSales - target) : 0;

      System.out.println("Your performance is " + performance
            + "\nYour bonus: " + bonus);
   }
}
