package school.lesson2.control;

public class ReturnDemo {

   public static void main(String[] args) {

      String[] names = { "Tom", "Alice", "Bob", "John", "Alex", "Don", "Carol" };
      System.out.println(foundMiscreant(names));
   }

   static String foundMiscreant(String[] people) {
      for (int i = 0; i < people.length; i++) {
         if (people[i].equals("Don")) {
            System.out.print("Found criminal ");
            return "Don";
         }
         if (people[i].equals("John")) {
            System.out.print("Found criminal ");
            return "John";
         }
      }
      return "";
   }
}
