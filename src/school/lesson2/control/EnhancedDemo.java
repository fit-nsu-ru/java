package school.lesson2.control;

public class EnhancedDemo {

   public static void main(String[] args) {

      String[] fruitArray = { "Apple", "Grapes", "Mango", "Orange", "Melon", "Kiwi" };

      for (String a : fruitArray) {

         System.out.println(a);
      }
   }
}
