package school.lesson2.control;

import java.io.IOException;

public class SwitchDemo {

   public static void main(String[] args) throws IOException {

      System.out.println("Enter your grade:");
      char grade = (char) System.in.read();

      switch (grade) {
      case 'A':
         System.out.println("Excellent!");
         break;
      case 'B':
         System.out.println("Well done");
         break;
      case 'C':
         System.out.println("You need to improve your grade");
         break;
      default:
         System.out.println("Invalid grade");
      }
      System.out.println("Your grade is " + grade);
   }
}
