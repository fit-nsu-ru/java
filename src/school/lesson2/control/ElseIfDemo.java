package school.lesson2.control;

import java.util.Scanner;

public class ElseIfDemo {
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
	
		System.out.println("Enter your sales:");
		double yourSales = in.nextDouble();
		System.out.println("Enter your target:");		
		double target = in.nextDouble();
		
		in.close();
		
		String performance;
		double bonus;

		if (yourSales >= 2 * target) {
			performance = "Excellent";
			bonus = 1000;
		} else if (yourSales >= 1.5 * target) {
			performance = "Fine";
			bonus = 500;
		} else if (yourSales >= target) {
			performance = "Satisfactory";
			bonus = 100;
		} else {
			performance = "Unsatisfactory";
			bonus = 0;
		}

		System.out.println("Your performance is " + performance
				+ "\nYour bonus: " + bonus);
	}
}
