package school.lesson2.control;

import java.util.Scanner;

public class IfDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      in.close();
      
      System.out.println("Enter your sales:");
      double yourSales = in.nextDouble();
      System.out.println("Enter your target:");
      double target = in.nextDouble();

      if (yourSales >= target) {

         double bonus = 10 + 0.05 * (yourSales - target);

         System.out.println("Your performance is Satifactory\n"
               + "Your bonus: " + bonus);
      }
   }
}
