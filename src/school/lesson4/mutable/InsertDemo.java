package school.lesson4.mutable;

public class InsertDemo {

	public static void main(String args[]) {
	   
		StringBuffer sb = new StringBuffer("I Java!");
	
		System.out.println("Before insert: " + sb);
		sb.insert(2, "like ");
		
		System.out.println("After insert: " + sb);
	}
}