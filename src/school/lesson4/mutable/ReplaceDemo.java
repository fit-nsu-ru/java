package school.lesson4.mutable;

public class ReplaceDemo {

	public static void main(String args[]) {
	   
	   String text = "This is a test. This is, too.";
	   StringBuffer sb = new StringBuffer(text);
	   
	   System.out.println("Text before replace: " + sb);
      sb.replace(5, 7, "was"); 
      sb.replace(22, 24, "was");
      
      System.out.println("Text after replace: " + sb);
	}
}