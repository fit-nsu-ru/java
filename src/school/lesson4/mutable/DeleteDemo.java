package school.lesson4.mutable;

public class DeleteDemo {

   public static void main(String args[]) {

      String text = "This is a test. This is, too.";
      StringBuffer sb = new StringBuffer(text);

      System.out.println("Text before delete: " + sb);
      
      sb.delete(5, 7);
      sb.delete(19, 21);
      
      System.out.println("Text after delete: " + sb);

      sb.insert(5, "was");
      sb.insert(22, "was");
      
      System.out.println("Text after insert: " + sb);
      
   }
}