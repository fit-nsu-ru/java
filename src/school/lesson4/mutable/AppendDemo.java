package school.lesson4.mutable;

public class AppendDemo {

   public static void main(String args[]) {

      StringBuffer sb = new StringBuffer();

      String hello = "Hello";
      String space = " ";
      String world = "World";
      String exclamation = "!";

      String s = sb.append(hello).append(space).append(world).append(exclamation).toString();
      System.out.println(s);

   }
}