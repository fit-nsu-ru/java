package school.lesson4.mutable;

public class CapacityCompare {
  
   public static void main(String[] args) {
           
      StringBuilder sb = new StringBuilder();
      long now = System.currentTimeMillis();
            
      for (int i = 0; i < 10000000; i++)
          sb.append("*");
          
      System.out.println("Time using default capacity: " + (System.currentTimeMillis() - now) + " ms");

      now = System.currentTimeMillis();
      
      sb = new StringBuilder(10000000);
       
      for (int i = 0; i < 10000000; i++)
         sb.append("*");
       
      System.out.println("Time using capacity 10000000: " + (System.currentTimeMillis() - now) + " ms");
   }
}
