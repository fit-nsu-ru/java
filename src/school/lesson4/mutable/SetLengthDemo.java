package school.lesson4.mutable;


public class SetLengthDemo {
   public static void main(String[] args) {

      StringBuilder sb = new StringBuilder("Hello world!");
      System.out.println("length = " + sb.length());
      System.out.println("capacity = " + sb.capacity());
      System.out.println("string = " + sb);
      
      
      System.out.println("Increasing capacity...");
      
      sb.ensureCapacity(30);
      System.out.println("length = " + sb.length());
      System.out.println("capacity = " + sb.capacity());
      System.out.println("string = " + sb);
       
      System.out.println("Trimming...");
      
      sb.trimToSize();
      System.out.println("length = " + sb.length());
      System.out.println("capacity = " + sb.capacity());
      System.out.println("string = " + sb);
   }
}