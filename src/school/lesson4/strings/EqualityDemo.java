package school.lesson4.strings;

public class EqualityDemo {
  
   public static void main(String[] args) {
     
     String greeting = new String("Hello");
     String hello = "Hello";
     
     System.out.println("References are equal : " + (greeting == hello));
     System.out.println("The same character sequence : " + greeting.equals(hello));
     
     System.out.println();
     
     greeting = new String("hello");
     hello = "Hello";
     
     System.out.println("References are equal : " + (greeting == hello));
     System.out.println("Same characters : " + greeting.equals(hello));
     System.out.println("Same character ignore case : " + greeting.equalsIgnoreCase(hello));
   }
}
