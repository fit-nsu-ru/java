package school.lesson4.strings;

import java.util.Arrays;

public class BubbleDemo {

   public static void main(String[] args) {

      String[] fruits = { "orange", "banana", "kiwi", "melon", "grapes", "apple" };

      System.out.println(Arrays.toString(fruits));

      
      String temp;

      for (int i = 1; i < fruits.length; i++) {
         for (int j = 0; j < fruits.length - i; j++) {

            if (fruits[j].compareTo(fruits[j + 1]) > 0) {
               temp = fruits[j + 1];
               fruits[j + 1] = fruits[j];
               fruits[j] = temp;
            }
         }
      }

     // Arrays.sort(fruits);
     
      System.out.println(Arrays.toString(fruits));
   }
}
