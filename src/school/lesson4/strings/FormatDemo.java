package school.lesson4.strings;

public class FormatDemo {

	public static void main(String[] args) {
		

		String fs;
		double doubleVar = 2.767;
		int intVar = 23423;
		String stringVar = "Hello";

		fs = String.format("The value of the double " +
		                   "variable is %f, while " +
		                   "the value of the " + 
		                   "integer variable is %d, " +
		                   " and the string is %s",
		                   doubleVar, intVar, stringVar);

		System.out.println(fs); 

		
		
	}

}
