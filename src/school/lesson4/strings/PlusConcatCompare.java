package school.lesson4.strings;

public class PlusConcatCompare {
  
   public static void main(String[] args) {
           
      String s = "";
      long now = System.currentTimeMillis();
            
      for (int i = 0; i < 100000; i++)
          s += "*";
          
      System.out.println("Time using plus: " + (System.currentTimeMillis() - now) + " ms");

      now = System.currentTimeMillis();
      
      s = "";
      
      for (int i = 0; i < 100000; i++)
         s = s.concat("*");
      
      System.out.println("Time using concat: " + (System.currentTimeMillis() - now) + " ms");
   }
}
