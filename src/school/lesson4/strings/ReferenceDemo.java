package school.lesson4.strings;

public class ReferenceDemo {

   public static void main(String[] args) {
      
      String greeting = "Hello"; 
      String worldgreeting = "Hello world!";
      
      if (greeting == "Hello") 
         System.out.println("Hello is equal to Hello");
      else 
         System.out.println("Hello is not equal to Hello");
            
      if (worldgreeting.substring(0, 5) == "Hello") 
         System.out.println("Hello is equal to Hello");
      else 
         System.out.println("Hello is not equal to Hello");
      
      if (greeting == "H"+"ello") 
         System.out.println("Hello is equal to Hello");
      else 
         System.out.println("Hello is not equal to Hello");
   }
}
