package school.lesson4.regex;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class MatchDemo {

   public static void main(String[] args) {

      Pattern pattern = Pattern.compile("\\d{2}");
      Matcher matcher = pattern.matcher("sdf121sdf30sdf4sd56dfs7");

      while (matcher.find()) {
         System.out.printf(
               "I found the text \"%s\" starting at index %d and ending "
                     + "at index %d.%n", matcher.group(), matcher.start(),
               matcher.end());
      }
   }
}
