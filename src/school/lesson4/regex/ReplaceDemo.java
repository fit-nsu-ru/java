package school.lesson4.regex;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ReplaceDemo {

   public static void main(String[] args) {

      String text = " A separator Text separator With separator Many separator Separators";
      String patternString = "separator";

      Pattern pattern = Pattern.compile(patternString);
      Matcher matcher = pattern.matcher(text);
      
      String replaced = matcher.replaceAll("!");
      System.out.println("Initial text: " + text);
      System.out.println("Text after replace: " + replaced);
    }
}
