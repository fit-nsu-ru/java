package school.lesson4.regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class SplitCompexDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      while (true) {

         System.out.println("Enter your regex: ");
         String pstring = in.nextLine();
         System.out.println("Enter input string to split: ");
         String mstring = in.nextLine();

         Pattern pattern = Pattern.compile(pstring);
         String[] chunks = pattern.split(mstring); 
         
         for( String s : chunks){
            
            System.out.println(s);
         }
      }
   }
}
