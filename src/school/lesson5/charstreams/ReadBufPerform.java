package school.lesson5.charstreams;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class ReadBufPerform {

   public static void main(String[] args) throws IOException {

      BufferedReader inbuf = null;
      FileReader in = null;

      int temp;

      long time = System.currentTimeMillis();
      try {
         inbuf = new BufferedReader(new FileReader("I:\\FileIO\\outbuf.txt"));

         for (int i = 0; i < 10000000; i++) {
            temp = inbuf.read();
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (inbuf != null)
               inbuf.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Buffered input time: " + time);

      time = System.currentTimeMillis();
      try {
         in = new FileReader("I:\\FileIO\\outnobuf.txt");

         for (int i = 0; i < 10000000; i++) {
            temp = in.read();
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (in != null)
               in.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Non-buffered input time: " + time);
   }
}
