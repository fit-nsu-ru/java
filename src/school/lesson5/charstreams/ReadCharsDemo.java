package school.lesson5.charstreams;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReadCharsDemo {

   public static void main(String[] args) throws IOException {

      FileReader in = null;
     
      try {         
         in = new FileReader("I:\\FileIO\\charfile.txt");
         char[] chars = new char[64];
         int nread = in.read(chars);
         if (nread > 0) {
            System.out.println("Read " + nread + " characters");
            System.out.println(Arrays.toString(chars));
         }

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (in != null)
               in.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
