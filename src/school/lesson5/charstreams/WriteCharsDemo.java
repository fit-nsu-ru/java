package school.lesson5.charstreams;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class WriteCharsDemo {

   public static void main(String[] args) {

      FileWriter out = null;
      char[] chars = new char[64];

      for (int i = 0; i < 64; i++) {
         chars[i] = (char) (0x0410 + i);
      }
      
      System.out.println(Arrays.toString(chars));
      
      try {
         out = new FileWriter("I:\\FileIO\\charsfile.txt");
         out.write(chars);
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
