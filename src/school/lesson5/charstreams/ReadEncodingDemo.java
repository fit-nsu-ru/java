package school.lesson5.charstreams;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.io.Reader;

public class ReadEncodingDemo {

   public static void main(String[] args) throws Exception {

      FileInputStream is = new FileInputStream("I:\\FileIO\\writeutf16.txt");
      Reader in = new InputStreamReader(is, "UTF-16");

      int data = in.read();
      while (data != -1) {
         char dataChar = (char) data;
         data = in.read();
         System.out.print(dataChar);
      }
      in.close();
   }
}
