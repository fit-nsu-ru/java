package school.lesson5.charstreams;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

public class WriteStringDemo {

   public static void main(String[] args) throws IOException {

      String source = "Hello World!";
      FileWriter out = null;

      System.out.println("String to write: " + source );
      System.out.println("Default Charset (Encoding): " + Charset.defaultCharset());
      System.out.println("String bytes using default encoding: " + Arrays.toString(source.getBytes()));
      
      try {
         out = new FileWriter("I:\\FileIO\\stringfile.txt");
         out.write(source);        
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
