package school.lesson5.charstreams;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class WriteBufPerform {

   public static void main(String[] args) throws IOException {

      BufferedWriter outbuf = null;
      FileWriter out = null;

      long time = System.currentTimeMillis();
      try {
         outbuf = new BufferedWriter(new FileWriter(
               "I:\\FileIO\\outbuf.txt"));

         for (int i = 0; i < 10000000; i++) {
            outbuf.write(65);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (outbuf != null)
                  outbuf.close();
         } catch (Exception e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Buffered output time: " + time);

      time = System.currentTimeMillis();
      try {
         out = new FileWriter("I:\\FileIO\\outnobuf.txt");

         for (int i = 0; i < 10000000; i++) {
            out.write(65);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Non-buffered output time: " + time);
   }
}
