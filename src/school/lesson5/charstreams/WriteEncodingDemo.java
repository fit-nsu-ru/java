package school.lesson5.charstreams;

import java.io.Writer;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

public class WriteEncodingDemo {

   public static void main(String[] args) throws UnsupportedEncodingException{

      String source = "Hello World!";
      Writer out = null;
 
      System.out.println("String to write: " + source );
      System.out.println("String bytes using UTF-16: " + Arrays.toString(source.getBytes("UTF-16")));
      
      try {

         FileOutputStream os = new FileOutputStream("I:\\FileIO\\writeutf16.txt");
         out = new OutputStreamWriter(os, "UTF-16");

         out.write(source);
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
