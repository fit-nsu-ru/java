package school.lesson5.charstreams;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class WriteFlushDemo {

   public static void main(String[] args) throws IOException {

      String source = "Hello World!";

      BufferedWriter out1 = null;
      BufferedWriter out2 = null;

      System.out.println("String to write: " + source );
      
      try {
         out1 = new BufferedWriter(new FileWriter("I:\\FileIO\\file1.txt"));
         out2 = new BufferedWriter(new FileWriter("I:\\FileIO\\file2.txt"));
                  
         out1.write(source);
         out2.write(source);
         
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out1 != null)
               out1.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
