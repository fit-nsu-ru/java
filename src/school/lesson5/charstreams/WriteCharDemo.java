package school.lesson5.charstreams;

import java.io.FileWriter;
import java.io.IOException;

public class WriteCharDemo {

   public static void main(String[] args) {

      FileWriter out = null;

      try {
         out = new FileWriter("I:\\FileIO\\charfile.txt");
         for (int i = 0x0410; i < 0x0450; i++) {
            System.out.print((char)i);
            out.write(i);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
