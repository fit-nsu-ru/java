package school.lesson5.charstreams;

import java.io.FileReader;
import java.io.IOException;

public class ReadCharDemo {

   public static void main(String[] args) {
            
      FileReader in = null;
      int temp;
      
      try {
         in = new FileReader("I:\\FileIO\\charfile.txt");
         for (int i = 0; i < 64; i++) {
            temp = in.read();
            if (temp == -1)
               break;

            System.out.print((char)temp);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (in != null)
               in.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
