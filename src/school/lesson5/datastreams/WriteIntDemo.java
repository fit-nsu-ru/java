package school.lesson5.datastreams;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteIntDemo {

   public static void main(String[] args) {

      String file = "I:\\FileIO\\writeint.dat";

      FileOutputStream fos = null;
      DataOutputStream dos = null;

      try {

         fos = new FileOutputStream(file);
         dos = new DataOutputStream(fos);

         for (int i = 0; i < 64; i++) {
            dos.writeInt(i);
            System.out.println(Integer.toBinaryString(i) + " ");
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dos != null) {
               dos.close();
            }
         }
         catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
