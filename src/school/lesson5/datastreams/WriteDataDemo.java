package school.lesson5.datastreams;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteDataDemo {

   public static void main(String[] args) {

      String file = "I:\\FileIO\\writedata.dat";

      FileOutputStream fos = null;
      DataOutputStream dos = null;

      String name = "Harry Hacker";
      char gender = 'm';
      boolean isMarried = true;
      byte numChildren = 2;
      short yearOfBirth = 1987;
      int salary = 30000;
      long netAsset = 8234567890L;
      double weight = 88.88;
      float gpa = 4.58f;

      System.out.println("Name: " + name);
      System.out.println("Gender: " + gender);
      System.out.println("Is married: " + isMarried);
      System.out.println("Number of children: " + numChildren);
      System.out.println("Year of birth: " + yearOfBirth);
      System.out.println("Salary: " + salary);
      System.out.println("Net Asset: " + netAsset);
      System.out.println("Weight: " + weight);
      System.out.println("GPA: " + gpa);

      try {

         fos = new FileOutputStream(file);
         dos = new DataOutputStream(fos);
         
         dos.writeUTF(name);
         dos.writeChar(gender);
         dos.writeBoolean(isMarried);
         dos.writeByte(numChildren);
         dos.writeShort(yearOfBirth);
         dos.writeInt(salary);
         dos.writeLong(netAsset);
         dos.writeDouble(weight);
         dos.writeFloat(gpa);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dos != null) {
               dos.close();
            }
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
