package datastreams;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;

import java.io.IOException;

public class DataDemo {

   public static void main(String[] args) {

      Employee bob = new Employee("Robert", (short) 1987, 'M', true, 30000);
      System.out.println(bob);
      bob.writeState("I:\\FileIO\\bob.dat");

      Employee robert = new Employee();
      System.out.println(robert);
      
      robert.readState("I:\\FileIO\\bob.dat");
      System.out.println(robert);
   }
}

class Employee {

   public Employee() {
   }

   public Employee(String name, short yearOfBirth, char gender,
         boolean isMarried, int salary) {

      this.name = name;
      this.yearOfBirth = yearOfBirth;
      this.gender = gender;
      this.isMarried = isMarried;
      this.salary = salary;
   }

   public String toString() {
      return "Employee [name=" + name + ", yearOfBirth=" + yearOfBirth
            + ", gender=" + gender + ", isMarried=" + isMarried + ", salary="
            + salary + "]";
   }

   public void writeState(String file) {

      FileOutputStream fos = null;
      DataOutputStream dos = null;

      try {

         fos = new FileOutputStream(file);
         dos = new DataOutputStream(fos);

         dos.writeUTF(name);
         dos.writeShort(yearOfBirth);
         dos.writeChar(gender);
         dos.writeBoolean(isMarried);
         dos.writeInt(salary);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dos != null) {
               dos.close();
            }
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }

   public void readState(String file) {

      FileInputStream fis = null;
      DataInputStream dis = null;

      try {

         fis = new FileInputStream(file);
         dis = new DataInputStream(fis);

         name = dis.readUTF();
         yearOfBirth = dis.readShort();
         gender = dis.readChar();
         isMarried = dis.readBoolean();
         salary = dis.readInt();

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dis != null) {
               dis.close();
            }
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }

   private String name;
   private short yearOfBirth;
   private char gender;
   private boolean isMarried;
   private int salary;
}
