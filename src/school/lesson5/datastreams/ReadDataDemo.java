package school.lesson5.datastreams;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadDataDemo {

   public static void main(String[] args) {

      String file = "I:\\FileIO\\writedata.dat";

      FileInputStream fis = null;
      DataInputStream dis = null;

      try {

         fis = new FileInputStream(file);
         dis = new DataInputStream(fis);

         String name = dis.readUTF();
         char gender = dis.readChar();
         boolean isMarried = dis.readBoolean();
         byte numChildren = dis.readByte();
         short yearOfBirth = dis.readShort();
         int salary = dis.readInt();
         long netAsset = dis.readLong();
         double weight = dis.readDouble();
         float gpa = dis.readFloat();

         System.out.println("Name: " + name);
         System.out.println("Gender: " + gender);
         System.out.println("Is married: " + isMarried);
         System.out.println("Number of children: " + numChildren);
         System.out.println("Year of birth: " + yearOfBirth);
         System.out.println("Salary: " + salary);
         System.out.println("Net Asset: " + netAsset);
         System.out.println("Weight: " + weight);
         System.out.println("GPA: " + gpa);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dis != null) {
               dis.close();
            }
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}