package school.lesson5.datastreams;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadIntDemo {

   public static void main(String[] args) {

      String file = "I:\\FileIO\\WriteInt.dat";
      
      FileInputStream fis = null;
      DataInputStream dis = null;
      
      try {

         fis = new FileInputStream(file);
         dis = new DataInputStream(fis);

          int temp;
         
         for (int i = 0; i < 64; i++) {
            temp = dis.readInt();
            
            System.out.println(temp);
            
            //System.out.println(Integer.toBinaryString(i) + " ");
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (dis != null) {
               dis.close();
            }
         }
         catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}