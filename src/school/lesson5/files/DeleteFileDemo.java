package school.lesson5.files;

import java.io.File;

public class DeleteFileDemo {
   public static void main(String[] args) {
      try {

         File file = new File("F:\\FileIO\\newfile.txt");

         if (file.delete()) {
            System.out.println(file.getName() + " was deleted!");
         } else {
            System.out.println("Delete operation failed.");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}