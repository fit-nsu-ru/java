package school.lesson5.files;

import java.io.File;

public class RenameFileDemo {

   public static void main(String[] args) {

      File oldfile = new File("F:\\FileIO\\somefile.txt");
      File newfile = new File("F:\\FileIO\\newfile.txt");

      if (oldfile.renameTo(newfile)) {
         System.out.println("Rename succesful " + oldfile.getName() + " was renamed to " + newfile.getName());
      } else {
         System.out.println("Rename failed");
      }
   }
}