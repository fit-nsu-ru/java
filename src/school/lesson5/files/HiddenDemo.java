package school.lesson5.files;

import java.io.File;

public class HiddenDemo {

 
   public static void main(String[] args) {
      File file = new File("F:\\FileIO\\somefile.txt");

      if (file.isHidden()) {
         System.out.println("This file is hidden");
      } else {
         System.out.println("This file is not hidden");
      }

   }

}
