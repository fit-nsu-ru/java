package school.lesson5.files;

import java.io.File;

public class AttributesDemo {
   
   public static void main(String[] args) {
      File file = new File("I:\\FileIO\\somefile.txt");

      if (file.isHidden()) {
         System.out.println("This file is hidden");
      } else {
         System.out.println("This file is not hidden");
      }
            
      if (file.canRead()) {
         System.out.println("This file is readable");
      } else {
         System.out.println("This file is not readable");
      }
      
      if (file.canWrite()) {
         System.out.println("This file is writable");
      } else {
         System.out.println("This file is not writable");
      }
      
      if (file.canExecute()) {
         System.out.println("This file is executable");
      } else {
         System.out.println("This file is not executable");
      }

   }

}
