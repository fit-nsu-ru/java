package school.lesson5.files;

import java.io.File;

public class ListDirDemo {

	public static void main(String args[]) {
		
		String dirname = "F:\\eclipse";
		File file = new File(dirname);
		
		if (file.isDirectory()) {
			System.out.println("Directory of " + dirname);
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				
				if (files[i].isDirectory()) {
					System.out.println(files[i].getAbsolutePath() + " is a directory");
				} else {
					System.out.print(files[i] + " is a file ");
					System.out.println( " is hidden? : " + files[i].isHidden());
				}
			}
		} else {
			System.out.println(dirname + " is not a directory");
		}
	}
}
