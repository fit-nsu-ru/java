package school.lesson5.files;

import java.io.File;

public class SetAttributesDemo {

   public static void main(String[] args) {

      File file = new File("F:\\FileIO\\somefile.txt");

           
      file.setWritable(true);
      //file.setReadOnly();
      //file.setExecutable(false);
      
      
      if (file.canRead()) {
         System.out.println("This file is readable");
      } else {
         System.out.println("This file is not readable");
      }
      
      if (file.canWrite()) {
         System.out.println("This file is writable");
      } else {
         System.out.println("This file is not writable");
      }
      
      if (file.canExecute()) {
         System.out.println("This file is executable");
      } else {
         System.out.println("This file is not executable");
      }
        
   }

}
