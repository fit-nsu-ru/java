package school.lesson5.files;

import java.io.File;

public class FileNameDemo {

   public static void main(String[] args) {

      try {

         File file = new File("Hello World!");
         System.out.println("File \""+ file.getPath() +"\" exists? " + file.exists());

      } catch (Exception e) {

         e.printStackTrace();
      }
   }
}
