package school.lesson5.files;

import java.io.File;

public class CreateFileDemo {

   public static void main(String[] args) {

      try {

         File file = new File("I:\\FileIO\\somefile.txt");

         if (file.createNewFile()) {
            System.out.println("File " + file.getName() + " was created");
         } else {
            System.out.println("Create operation failed.");
         }

      } catch (Exception e) {

         e.printStackTrace();
      }
   }
}
