package school.lesson5.files;

import java.io.File;

public class CreateDirDemo {

   public static void main(String args[]) {

      String dirname = "I:\\FileIO";
      File dir = new File(dirname);
      
      if (dir.exists()) {
         System.out.println("Directory of " + dirname +" exists");
      }
      else {
         System.out.println("Directory of " + dirname +" does not exist");
      }
      
      if (dir.mkdir()) {
         System.out.println("Directory " + dir.getName() + " was created");
      } else {
         System.out.println("Create operation failed.");
      }
   }
}
