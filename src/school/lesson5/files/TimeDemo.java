package school.lesson5.files;

import java.io.File;
import java.text.SimpleDateFormat;

public class TimeDemo {

   public static void main(String[] args) {
      
      File file = new File("F:\\FileIO\\somefile.txt");

      System.out.println("Before Format : " + file.lastModified());

      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

      System.out.println("After Format : " + sdf.format(file.lastModified()));
   }
}
