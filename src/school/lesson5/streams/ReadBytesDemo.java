package school.lesson5.streams;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class ReadBytesDemo {

   public static void main(String[] args) throws IOException {

      FileInputStream in = null;
      byte[] bytes = new byte[256];

      try {
         in = new FileInputStream("I:\\FileIO\\bytesfile.dat");
         in.read(bytes);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (in != null)
               in.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(bytes));
   }
}
