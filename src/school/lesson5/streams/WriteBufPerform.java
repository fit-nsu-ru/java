package school.lesson5.streams;

import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

public class WriteBufPerform {

   public static void main(String[] args) throws IOException {

      BufferedOutputStream outbuf = null;
      FileOutputStream out = null;

      long time = System.currentTimeMillis();
      try {
         outbuf = new BufferedOutputStream(new FileOutputStream(
               "I:\\FileIO\\outbuf.dat"));

         for (int i = 0; i < 10000000; i++) {
            outbuf.write(65);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (outbuf != null)
               outbuf.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Buffered output time: " + time);

      time = System.currentTimeMillis();
      try {
         out = new FileOutputStream("I:\\FileIO\\outnobuf.dat");

         for (int i = 0; i < 10000000; i++) {
            out.write(65);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      time = System.currentTimeMillis() - time;
      System.out.println("Non-buffered output time: " + time);
   }
}
