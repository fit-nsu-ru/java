package school.lesson5.streams;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class ReadByteDemo {

   public static void main(String[] args) throws IOException {
            
      FileInputStream in = null;
      int[] ints = new int[256];
      int temp;
      
      try {
         in = new FileInputStream("I:\\FileIO\\bytesfile.dat");
         for (int i = 0; i < 256; i++) {
            temp = in.read();
            if (temp == -1)
               break;

            ints[i] = temp;
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (in != null)
               in.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(ints));
   }
}
