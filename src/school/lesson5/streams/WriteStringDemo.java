package school.lesson5.streams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class WriteStringDemo {

   public static void main(String[] args) throws IOException {

      String source = "Hello World!";
      byte[] bytes = source.getBytes();

      FileOutputStream out = null;

      try {
         out = new FileOutputStream("I:\\FileIO\\stringfile.dat");
         out.write(bytes);
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(bytes));
   }
}
