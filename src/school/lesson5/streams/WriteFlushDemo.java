package school.lesson5.streams;

import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class WriteFlushDemo {

   public static void main(String[] args) throws IOException {

      String source = "Hello World!";
      byte[] bytes = source.getBytes();

      BufferedOutputStream out1 = null;
      BufferedOutputStream out2 = null;
      
      try {
         out1 = new BufferedOutputStream(new FileOutputStream("I:\\FileIO\\file1.dat"));
         out2 = new BufferedOutputStream(new FileOutputStream("I:\\FileIO\\file2.dat"));
                  
         out1.write(bytes);
         out2.write(bytes);
         
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out1 != null)
               out1.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(bytes));
   }
}
