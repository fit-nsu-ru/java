package school.lesson5.streams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class WriteBytesDemo {

   public static void main(String[] args) throws IOException {

      FileOutputStream out = null;
      byte[] bytes = new byte[256];

      for (int i = 0; i < 256; i++) {
         bytes[i] = (byte) i;
      }
     
      try {
         out = new FileOutputStream("I:\\FileIO\\bytesfile.dat");
         out.write(bytes);
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(bytes));
   }
}
