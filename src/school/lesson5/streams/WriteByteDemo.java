package school.lesson5.streams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class WriteByteDemo {

   public static void main(String[] args) {

      FileOutputStream out = null;
      int[] ints = new int[256];

      try {
         out = new FileOutputStream("I:\\FileIO\\bytesfile.dat");
         for (int i = 0; i < 256; i++) {
            ints[i] = i;
            out.write(i);
         }
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      System.out.println(Arrays.toString(ints));
   }
}
