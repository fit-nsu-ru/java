package school.lesson5.streams;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileDemo {

   public static void main(String args[]) throws IOException {

      String finname = "I:\\FileIO\\input.txt";
      String foutname = "I:\\FileIO\\output.txt";

      FileInputStream fin = new FileInputStream(finname);
      FileOutputStream fout = new FileOutputStream(foutname);

      int i;
      do {
         i = fin.read();
         if (i != -1)
            fout.write(i);
      } while (i != -1);

      fin.close();
      fout.close();
      
      System.out.println("Done");
   }
}
