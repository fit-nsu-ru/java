package school.lesson5.serialization;

import java.io.*;


public class StringLinkedList implements Serializable {

    private transient int size = 0;
    private transient Entry head = null;

    private static class Entry {

        String data;
        Entry  next;
        Entry  previous;
    }

    public void add(String s) {

        Entry e = new Entry();
        e.data = s;
        e.next = head;

        if (head != null)
            head.previous = e;
        head = e;
        
        size++;
    }

    public String toString() {

        StringBuffer b = new StringBuffer();
        for (Entry e=head; e!=null; e = e.next) {
            b.append(e.data);
            b.append(" ");
        }
        return b.toString();
    }

    private void writeObject(ObjectOutputStream s) throws IOException {

        s.defaultWriteObject();
        s.writeInt(size);
        for (Entry e = head; e != null; e = e.next)
            s.writeObject(e.data);
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {

        s.defaultReadObject();
        int size = s.readInt();
        for (int i = 0; i < size; i++)
            add((String)s.readObject());
    }
}