package school.lesson5.serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

class Employee implements Serializable{

   public Employee() {
      System.out.println("Employee object is being created using a default constructor");
   }

   public Employee(String name, short yearOfBirth, char gender,
         boolean isMarried, int salary) {

      this.name = name;
      this.yearOfBirth = yearOfBirth;
      this.gender = gender;
      this.isMarried = isMarried;
      this.salary = salary;
      
      numEmployees++;
      System.out.println("Employee object is being created using a constructor");
   }

   public String toString() {
      return "Employee [name=" + name + ", yearOfBirth=" + yearOfBirth
            + ", gender=" + gender + ", isMarried=" + isMarried + ", salary="
            + salary + "]";
   }
   
   public static int getNumEmployees(){ 
      return numEmployees;
   }
   
   private String name;
   private short yearOfBirth;
   private char gender;
   private boolean isMarried;
   private int salary;
   
   private static int numEmployees = 0; 
}
