package school.lesson5.serialization;

import java.io.*;

public class StringArrayList implements Serializable {

    private transient int size   = 0;
    private transient String buf[] = new String[16];
  
    public void add(String s) {

        buf[size] = s;
        size++;
    }

    public String toString() {

        StringBuffer b = new StringBuffer();
        for (int i=0; i<size; i++) {
            b.append(buf[i]);
            b.append(" ");
        }
        return b.toString();
    }

    private void writeObject(ObjectOutputStream s) throws IOException {

        s.defaultWriteObject();
        s.writeInt(size);
        for (int i=0; i<size; i++)
            s.writeObject(buf[i]);
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {

        s.defaultReadObject();
        int size = s.readInt();

        for (int i = 0; i < size; i++)
            add((String)s.readObject());
    }
}