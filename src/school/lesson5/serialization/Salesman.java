package school.lesson5.serialization;

import java.io.Serializable;

class Salesman implements Serializable {

   public Salesman(String n, double s) {
      name = n;
      salary = s;
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

  public String toString() {
      return getClass().getSimpleName() + "[name=" + name + ",salary=" + salary
            + ",bonus=" + bonus + "]";
   }

   public void setBonus(double bonus) {
      this.bonus = bonus;
   }

   private String name;
   private double salary;

   private transient double bonus;
}