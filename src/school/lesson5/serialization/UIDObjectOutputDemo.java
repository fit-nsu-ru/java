package school.lesson5.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class UIDObjectOutputDemo {

   public static void main(String[] args) {

      ItemUID coffemaker = new ItemUID("Coffemaker", 2912);
      //ItemUID coffemaker = new ItemUID("Coffemaker", 2912, "Brews great coffee!");
      System.out.println(coffemaker);
      
      FileOutputStream fos = null;
      ObjectOutputStream oos = null;

      try {
         fos = new FileOutputStream("I:\\FileIO\\item.dat");
         oos = new ObjectOutputStream(fos);

         oos.writeObject(coffemaker);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (oos != null)
               oos.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
