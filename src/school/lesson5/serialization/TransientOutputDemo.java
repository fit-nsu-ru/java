package school.lesson5.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class TransientOutputDemo {

   public static void main(String[] args) {

      
      Salesman bob = new Salesman("Robert", 30000);
      bob.setBonus(10000);
      System.out.println(bob);
            
      FileOutputStream fos = null;
      ObjectOutputStream oos = null;

      try {
         fos = new FileOutputStream("I:\\FileIO\\salesman.dat");
         oos = new ObjectOutputStream(fos);

         oos.writeObject(bob);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (oos != null)
               oos.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
