package school.lesson5.serialization;

import java.io.Serializable;

public class ItemUID implements Serializable {

   private static final long serialVersionUID = -652131815957790254L;

   public ItemUID(String name, int number) {
      this.name = name;
      this.number = number;
   }

   /*
    * public ItemUID(String name, int number, String description) { this.name =
    * name; this.number = number; this.description = description; }
    */

   public String getName() {
      return name;
   }

   public int getNumber() {
      return number;
   }

   public String toString() {
      return "[name=" + name + ", number=" + number + "]";
      // return "[name=" + name + ", number=" + number + ", description=" +
      // description+"]";

   }

   private String name;
   private int number;
   // private String description;
}
