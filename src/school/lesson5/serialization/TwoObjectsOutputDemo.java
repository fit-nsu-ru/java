package school.lesson5.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class TwoObjectsOutputDemo {

   public static void main(String[] args) {

      Person ann = new Person("Ann");
      Person bob = new Person("Bob");
      ann.setSpouse(bob);
      bob.setSpouse(ann);

      System.out.println(ann);
      System.out.println(bob);

      FileOutputStream fos = null;
      ObjectOutputStream oos = null;

      try {
         fos = new FileOutputStream("I:\\FileIO\\person.dat");
         oos = new ObjectOutputStream(fos);

         oos.writeObject(ann);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (oos != null)
               oos.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
