package school.lesson5.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.BufferedOutputStream;

public class ObjectOutputDemo {

   public static void main(String[] args) {

      
      Employee bob = new Employee("Robert", (short) 1987, 'M', true, 30000);
      System.out.println(bob);
      System.out.println("Total number of employees: " + Employee.getNumEmployees()); 
      
      FileOutputStream fos = null;
      BufferedOutputStream bos = null;
      ObjectOutputStream oos = null;

      try {
         
         fos = new FileOutputStream("I:\\FileIO\\employee.dat");
         bos = new BufferedOutputStream(fos);
         oos = new ObjectOutputStream(bos);
       
         System.out.println("Employee object was serialized");
         oos.writeObject(bob);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (oos != null)
               oos.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
