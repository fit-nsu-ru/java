package school.lesson5.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class CustomOutputDemo {

   public static void main(String[] args) {

      
      StringLinkedList sList = new StringLinkedList();
      
      sList.add("apple");
      sList.add("orange");
      sList.add("banana");
      
      System.out.println("List contents are: " + sList);
      
      FileOutputStream fos = null;
      ObjectOutputStream oos = null;

      try {
         fos = new FileOutputStream("I:\\FileIO\\fruitlist.dat");
         oos = new ObjectOutputStream(fos);
         
         System.out.println("Serializing .....");
         oos.writeObject(sList);

      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (oos != null)
               oos.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
