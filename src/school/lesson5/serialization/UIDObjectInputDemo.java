package school.lesson5.serialization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;

class UIDObjectInputDemo {

   public static void main(String[] args) {

      FileInputStream fis = null;
      ObjectInputStream ois = null;

      try {
        
         fis = new FileInputStream("I:\\FileIO\\item.dat");
         ois = new ObjectInputStream(fis);
         
         ItemUID item = (ItemUID) ois.readObject();
         System.out.println(item);
         
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } catch (ClassNotFoundException e) {
         System.out.println("Class was not found");         
      }
      finally {
         try {
            if (ois != null)
               ois.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
   }
}
