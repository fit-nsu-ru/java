package school.lesson5.serialization;

import java.io.Serializable;

public class Person implements Serializable{

   public Person(String name) {
      this.name = name;
      System.out.println("Person constructor is called, name=" + name);
   }

   public Person() {
      this.name = "A person";
      System.out.println("Person default constructor is called, name="
            + name);
   }

   public String getName() {
      return name;
   }

   public void setSpouse(Person value) {
      spouse = value;
   }
   
   public Person getSpouse() {
      return spouse;
   }
      
   public String toString() {
      return "[Person: name=" + name + " spouse=" + spouse.getName() + "]";
   }

   private String name;
   private Person spouse;
}