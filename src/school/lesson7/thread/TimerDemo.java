package school.lesson7.thread;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerDemo {

    public static void main(String args[]) throws InterruptedException {

        Timer timer = new Timer();

        TimerTask r = new GreetingTask("Hello, World!");
        timer.schedule(r, 0, 1000);
    }
}

class GreetingTask extends TimerTask {

    private String greeting;

    public GreetingTask(String aGreeting) {
        greeting = aGreeting;
    }

    public void run() {
        Date now = new Date();
        System.out.println(now + " " + greeting);
    }
}