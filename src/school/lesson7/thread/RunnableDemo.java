package school.lesson7.thread;


public class RunnableDemo {

    public static void main(String[] args) {

        Runnable r1 = new GreetingRunnable("Hello, World!");
        Runnable r2 = new GreetingRunnable("Hi, World!");
      
        r1.run();
        r2.run();

        System.out.println("Runnables ran!");
    }
}
