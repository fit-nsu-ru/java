package school.lesson7.thread;

public class MultiThreadDemo {

    public static void main(String[] args) {

        String[] greetings = { "Hello, World!", "Hi, World!", "Hey, World!",
                "Yo, World!", "What's up" };

        for (String greeting : greetings) {

            new GreetingThread(greeting).start();
        }

        System.out.println("Threads started!");
    }
}
