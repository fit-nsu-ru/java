package school.lesson7.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadExecutorDemo {
   
    public static void main(String[] args) {
        
        Runnable r1 = new GreetingRunnable("Hello, World!");
        Runnable r2 = new GreetingRunnable("Hi, World!");
        
        ExecutorService single = Executors.newSingleThreadExecutor();

        single.execute(r1);
        single.execute(r2);
        
        System.out.println("Single executor launched!");
    }
}
