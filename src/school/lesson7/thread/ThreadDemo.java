package school.lesson7.thread;


public class ThreadDemo {
    
    public static void main(String[] args) {
        
        Thread t1 = new GreetingThread("Hello, World!");
        Thread t2 = new GreetingThread("Hi, World!");
        
        t1.start();
        t2.start();
        
        System.out.println("Threads started!");
    }
}
