package school.lesson7.thread;

import java.util.Date;

class GreetingThread extends Thread {
    
    private static final int REPETITIONS = 5;
    private static final int DELAY = 1000;

    private String greeting;

    public GreetingThread(String aGreeting) {
        greeting = aGreeting;
    }

    public void run() {
        try {
            for (int i = 1; i <= REPETITIONS; i++) {
                Date now = new Date();
                System.out.println(now + " " + greeting);
                Thread.sleep(DELAY);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}