package school.lesson7.synch;

public class BillingDemo {

    public static void main(String[] args) {

        BillingAccount account = new BillingAccount(400.00);

        Runnable p1 = new PayBillRunnable(account, 400.00);
        Runnable p2 = new PayBillRunnable(account, 400.00);

        Thread t1 = new Thread(p1);
        Thread t2 = new Thread(p2);

        t1.start();
        t2.start();
    }
}

class BillingAccount {
    private double balance;

    public BillingAccount(double balance) {
        this.balance = balance;
    }

    public void pay(double amount) {

        System.out.println("Paying: " + amount);
        balance = balance - amount;
        System.out.println("Current Balance: " + balance);
    }

    public double getBalance() {
        return balance;
    }
}

class PayBillRunnable implements Runnable {
    double amount;
    BillingAccount account;

    public PayBillRunnable(BillingAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {

        synchronized (account) {

            if (account.getBalance() >= amount) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.pay(amount);
            }
        }
    }
}
