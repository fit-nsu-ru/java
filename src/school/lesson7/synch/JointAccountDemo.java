package school.lesson7.synch;

class JointAccount {
    private double balance;

    public JointAccount(double balance) {
        this.balance = balance;
    }

    public void process(double amount) {
        double temp = this.balance + amount;

        if (amount < 0)
            System.out.println("Withdrawing: " + Math.abs(amount));
        else
            System.out.println("Depositing: " + amount);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        balance = temp;
        System.out.println("Current Balance: " + balance);
    }
}

class ProcessRunnable implements Runnable {
    double amount;
    JointAccount account;

    public ProcessRunnable(JointAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {
        account.process(amount);
    }
}

public class JointAccountDemo {

    public static void main(String[] args) {
        
        JointAccount account = new JointAccount(400.00);
        
        Runnable d = new ProcessRunnable(account, -400.00);
        Runnable w = new ProcessRunnable(account, 1000.00);
      
        Thread td = new Thread(d);
        Thread tw = new Thread(w);

        td.start();
        tw.start();
    }
}