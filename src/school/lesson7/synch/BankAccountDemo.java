package school.lesson7.synch;

public class BankAccountDemo {

    public static void main(String[] args) {
        BankAccount account = new BankAccount(400.00);

        Runnable d = new WithdrawRunnable(account, 400.00);
        Runnable w = new DepositRunnable(account, 1000.00);

        Thread td = new Thread(d);
        Thread tw = new Thread(w);

        td.start();
        tw.start();
    }
}

class DepositRunnable implements Runnable {
    double amount;
    BankAccount account;

    public DepositRunnable(BankAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {
        account.deposit(amount);
    }
}

class WithdrawRunnable implements Runnable {
    double amount;
    BankAccount account;

    public WithdrawRunnable(BankAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {
        account.withdraw(amount);
    }
}

class BankAccount {
    private double balance;

    public BankAccount(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount) {
        double temp = this.balance + amount;
        System.out.println("Depositing: " + amount);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        balance = temp;
        System.out.println("Current Balance: " + balance);
    }

    public void withdraw(double amount) {
        double temp = this.balance - amount;
        System.out.println("Withdrawing: " + amount);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        balance = temp;
        System.out.println("Current Balance: " + balance);
    }

    public double getBalance() {
        return balance;
    }
}
