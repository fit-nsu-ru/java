package school.lesson7.synch;

public class SerialDemo {
   
    public static void main(String[] args) {
           
        double[] small = {2.35, 3.42, 1.49};
        double[] large = {49.99, 53.45, 50.24}; 
        
        Runnable s = new PrintRunnable(small);
        Runnable l = new PrintRunnable(large);
      
        Thread ts = new Thread(s);
        Thread tl = new Thread(l);

        ts.start();
        tl.start();
    }
}

class PrintRunnable implements Runnable {
    double[] amounts;
   
    public PrintRunnable(double[] amounts) {
        this.amounts = amounts;
    }

    public void run() {
     
        NumberGenerator generator = NumberGenerator.getInstance();
        
        for(double amount : amounts) {
      
            Check check = new Check (generator.getNextSerial(), amount);
            System.out.println(check);
        }
    }
}

class Check {
   
    private int number;
    private double amount;
    
    public Check( int number, double amount) {
        this.number = number;
        this.amount = amount;
    }

    public String toString() {
        return "Check [number=" + number + ", amount=" + amount + "]";
    }
}

class NumberGenerator {

    private int number;

    public synchronized int getNextSerial() {
        return ++number;
    }

    public synchronized static NumberGenerator getInstance() {
        if (instance == null) {
        
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            instance = new NumberGenerator();
        }
        return instance;
    }

    private NumberGenerator() {
    }

    private static NumberGenerator instance;
}