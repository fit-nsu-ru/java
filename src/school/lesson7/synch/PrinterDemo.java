package school.lesson7.synch;

class Printer {

    public void print(String message) {

        char[] chars = message.toCharArray();

        for (int k = 0; k < chars.length; k++) {

            System.out.print(chars[k]);
        }
        System.out.println();
    }

    public  void superPrint(String message) {

        char[] chars = message.toUpperCase().toCharArray();

        for (int k = 0; k < chars.length; k++) {

            System.out.print(chars[k]);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
        System.out.println();
    }
}

class WorkerThread extends Thread {

    private Printer printer;
    private String message;

    public WorkerThread(Printer printer, String message) {
        this.printer = printer;
        this.message = message;
    }

    public void run() {

       // for (int i = 0; i < 1; i++) {

            printer.print(message);
      //  }
    }
}

class SuperWorkerThread extends Thread {

    private Printer printer;
    private String message;

    public SuperWorkerThread(Printer printer, String message) {
        this.printer = printer;
        this.message = message;
    }

    public void run() {

        for (int i = 0; i < 20; i++) {

            synchronized (Printer.class) {

                printer.superPrint(message);
            }
        }
    }
}

public class PrinterDemo {

    public static void main(String[] args) {

        Printer printer = new Printer();

        Thread ping = new WorkerThread(printer, "Piiiiiiiiiiiing");
        Thread pong = new WorkerThread(printer, "Poooooooooooong");

        ping.start();
        pong.start();
    }
}
