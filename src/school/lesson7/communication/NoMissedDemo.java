package school.lesson7.communication;

class NoMissedSignal {

    boolean wasSignalled = false;

    public synchronized void doWait() {
        if (!wasSignalled) {
            System.out.println("Waiting for the signal");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Signal received");
        }
        else {
            System.out.println("Missed the signal, will not wait");
            
        }
        wasSignalled = false;
    }

    public synchronized void doNotify() {

        wasSignalled = true;
        notify();
    }
}

class NoMissedWaiterThread extends Thread {

    private NoMissedSignal signal;

    public NoMissedWaiterThread(NoMissedSignal signal) {
        this.signal = signal;
    }

    public void run() {
        synchronized (signal) {
            signal.doWait();
        }
    }
}

public class NoMissedDemo {

    public static void main(String s[]) {

        NoMissedSignal signal = new NoMissedSignal();
        Thread t = new NoMissedWaiterThread(signal);
     
        t.start();
        System.out.println("Thread: " + t.getName() + " was started.....");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        
        signal.doNotify();
        System.out.println("After notify. Thread: "
                + Thread.currentThread().getName());

    }
}