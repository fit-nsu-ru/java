package school.lesson7.communication;

public class BankAccountDemo {

    public static void main(String[] args) {
        BankAccount account = new BankAccount(500.00);

        Runnable d = new WithdrawRunnable(account, 1000.00);
        Runnable w = new DepositRunnable(account, 500.00);

        Thread tw = new Thread(w);
        Thread td = new Thread(d);
        
        tw.start();
        td.start();
    }
}

class DepositRunnable implements Runnable {
    double amount;
    BankAccount account;

    public DepositRunnable(BankAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        account.deposit(amount);
    }
}

class WithdrawRunnable implements Runnable {
    double amount;
    BankAccount account;

    public WithdrawRunnable(BankAccount account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public void run() {
        try {
            account.withdraw(amount);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class BankAccount {
    private double balance;

    public BankAccount(double balance) {
        this.balance = balance;
    }

    public synchronized void deposit(double amount) {
        
        double temp = this.balance + amount;
        System.out.println("Depositing: " + amount);
        balance = temp;
        System.out.println("Current Balance: " + balance);
        notifyAll();
    }

    public synchronized void withdraw(double amount)
            throws InterruptedException {

        System.out.println("About to check funds availability");
    
        while (this.balance < amount) {
            wait();
        }
        
        System.out.println("Funds available");
        
        double temp = this.balance - amount;
        System.out.println("Withdrawing: " + amount);
        balance = temp;
        System.out.println("Current Balance: " + balance);
    }

    public double getBalance() {
        return balance;
    }
}
