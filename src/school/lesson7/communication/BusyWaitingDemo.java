package school.lesson7.communication;

class Signal {

    private volatile boolean hasDataToProcess = false;
    private String dataToProcess = null;

    public synchronized String getDataToProcess() {
        return dataToProcess;
    }
    
    public synchronized void setDataToProcess(String dataToProcess) {
        this.dataToProcess = dataToProcess;
    }
    
    public synchronized boolean hasDataToProcess() {
        return this.hasDataToProcess;
    }

    public synchronized void setHasDataToProcess(boolean hasData) {
        this.hasDataToProcess = hasData;
    }
}

class BusyWaiterThread extends Thread {

    private Signal signalObject;

    public BusyWaiterThread(Signal signalObject) {
        this.signalObject = signalObject;
    }

    public void run() {

        while (!signalObject.hasDataToProcess()) {
            // do nothing... busy waiting
        }
        System.out.println("After wait. Thread: " + Thread.currentThread().getName());
        String data = signalObject.getDataToProcess();
        System.out.println("Receiving data :" + data);
    }
}

public class BusyWaitingDemo {

    public static void main(String s[]) {
        Signal signalObject = new Signal();

        for (int i = 0; i < 3; i++) {

            Thread t = new BusyWaiterThread(signalObject);
            t.start();
            System.out.println("Thread: " + t.getName() + " was started.....");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
        System.out.println("Before notify. Thread: "
                + Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }

        String data = "-IMPORTANT DATA-";
        System.out.println("Sending data :" + data);
        signalObject.setDataToProcess(data);
        signalObject.setHasDataToProcess(true);
    }
}
