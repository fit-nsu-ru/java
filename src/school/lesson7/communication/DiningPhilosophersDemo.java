package school.lesson7.communication;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DiningPhilosophersDemo {

    private static final int SIZE = 5;

    public static void main(String[] args) throws Exception {

        ExecutorService exec = Executors.newCachedThreadPool();
        Chopstick[] sticks = new Chopstick[SIZE];
        for (int i = 0; i < SIZE; i++)
            sticks[i] = new Chopstick();
        for (int i = 0; i < SIZE; i++) {
            if (i < (SIZE - 1)) {
                exec.execute(new Philosopher(sticks[i], sticks[(i + 1) % SIZE],
                        i));
            } else {
                exec.execute(new Philosopher(sticks[0], sticks[i - 1], i));
            }
        }
        System.out.println("Press 'Enter' to quit");
        System.in.read();
        exec.shutdownNow();
    }
}

class Philosopher implements Runnable {
    private Chopstick left;
    private Chopstick right;
    private final int id;
    private Random rand = new Random(35);

    private void pause() throws InterruptedException {
        Thread.sleep(rand.nextInt(1500));
    }

    public Philosopher(Chopstick left, Chopstick right, int ident) {
        this.left = left;
        this.right = right;
        id = ident;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                System.out.println(this + " " + "thinking");
                pause();
                System.out.println(this + " " + "grabbing right");
                right.take();
                pause();
                System.out.println(this + " " + "grabbing left");
                left.take();
                System.out.println(this + " " + "eating");
                pause();
                right.drop();
                left.drop();
            }
        } catch (InterruptedException e) {
            System.out.println(this + " " + "exiting via interrupt");
        }
    }

    public String toString() {
        return "Philosopher " + id;
    }
}

class Chopstick {
    private boolean taken = false;

    public synchronized void take() throws InterruptedException {
        while (taken)
            wait();
        taken = true;
    }

    public synchronized void drop() {
        taken = false;
        notify();
    }
}