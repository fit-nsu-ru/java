package school.lesson7.communication;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Table {

    private boolean pinged = false;

    public synchronized void doPing() throws InterruptedException {

        while (pinged == true) {
            wait();
        }
        System.out.println("-PING-");
        pinged = true;
        notify();
    }

    public synchronized void doPong() throws InterruptedException {
        while (pinged == false) {
            wait();
        }
        System.out.println("-PONG-");
        pinged = false;
        notify();
    }
}

class Ping implements Runnable {

    private Table table;

    public Ping(Table table) {
        this.table = table;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                table.doPing();
            }
        } catch (InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending Ping task");
    }
}

class Pong implements Runnable {

    private Table table;

    public Pong(Table table) {
        this.table = table;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                table.doPong();
            }
        } catch (InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending Pong task");
    }
}

public class PingPong {

    public static void main(String s[]) {

        Table table = new Table();
        Ping ping = new Ping(table);
        Pong pong = new Pong(table);

        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(ping);
        exec.execute(pong);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exec.shutdownNow();
    }
}