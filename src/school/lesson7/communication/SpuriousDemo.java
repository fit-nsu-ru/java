package school.lesson7.communication;

class WaitingThread extends Thread {
    public void run() {

        try {
            sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {

        }

        synchronized (this) {
            try {

                System.out.println("waiting");
                wait(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Thread " + currentThread().getName()
                + " was spontaneously awakened and exited wait()");
        System.exit(0);
    }
}

public class SpuriousDemo {

    public static void main(String[] args) {
        WaitingThread threads[] = new WaitingThread[1500];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new WaitingThread();

            threads[i].start();

            while (threads[i].getState() != Thread.State.TIMED_WAITING)
                Thread.yield();

            threads[i].interrupt();
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
