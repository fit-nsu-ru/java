package school.lesson7.communication;

class WaiterThread extends Thread {

    private Object sharedObject;

    public WaiterThread(Object sharedObject) {
        this.sharedObject = sharedObject;
    }

    public void run() {
        synchronized (sharedObject) {
            try {
                sharedObject.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("After wait. Thread: "
                    + Thread.currentThread().getName());
        }
    }
}

public class SimpleNotifyDemo {

    public static void main(String s[]) {
        Object sharedObject = new Object();

        for (int i = 0; i < 3; i++) {

            Thread t = new WaiterThread(sharedObject);
            t.start();
            System.out.println("Thread: " + t.getName() + " was started.....");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Before notify. Thread: "
                + Thread.currentThread().getName());
        synchronized (sharedObject) {
            sharedObject.notify();
        }
    }
}