package school.lesson7.communication;

public class NoSpuriousDemo {

    public static void main(String s[]) {
        NoSpuriousSignal signal = new NoSpuriousSignal();

        Thread t = new NoSpuriousWaiterThread(signal);
        t.start();
        System.out.println("Thread: " + t.getName() + " was started.....");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Before notify. Thread: "
                + Thread.currentThread().getName());
        signal.doNotify();
    }
}

class NoSpuriousWaiterThread extends Thread {

    private NoSpuriousSignal signal;

    public NoSpuriousWaiterThread(NoSpuriousSignal signal) {
        this.signal = signal;
    }

    public void run() {
        synchronized (signal) {
            signal.doWait();

            System.out.println("After wait. Thread: "
                    + Thread.currentThread().getName());
        }
    }
}

class NoSpuriousSignal {

    boolean wasSignalled = false;

    public synchronized void doWait() {
        if (!wasSignalled) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        wasSignalled = false;
    }

    public synchronized void doNotify() {

        wasSignalled = true;
        notify();
    }
}