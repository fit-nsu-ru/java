package school.lesson7.data;

import java.util.HashMap;
import java.util.Map;

public class ThreadMapDemo {

    public static void main(String[] args) {

        MapCountingPrinter printer = new MapCountingPrinter();

        Thread thread1 = new MapPrinterThread(printer, "Hello !");
        Thread thread2 = new MapPrinterThread(printer, "Yo !");

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MapCountingPrinter {
    
    private Map<String, Integer> counter = new HashMap<String,Integer>();
    
    public void print(String text) {

        System.out.println("Thread: " + Thread.currentThread().getName()
                + " printing: " + text);
        
        
        if (!counter.containsKey(Thread.currentThread().getName())) {
            counter.put(Thread.currentThread().getName(), 1);
        } else {
            counter.put(Thread.currentThread().getName(),
                    counter.get(Thread.currentThread().getName()) + 1);
        }
    }

    public int getCount() {
        return counter.get(Thread.currentThread().getName()).intValue();
    }
}

class MapPrinterThread extends Thread {

    private MapCountingPrinter printer;
    private String greeting;

    public MapPrinterThread(MapCountingPrinter printer, String greeting) {
        this.printer = printer;
        this.greeting = greeting;
    }

    public void run() {

        for (int i = 0; i < Math.random() * 10; i++) {
            printer.print(greeting);
        }
        System.out.println("Thread: " + getName() + " printed: "
                + printer.getCount() + " greetings");
    }
}
