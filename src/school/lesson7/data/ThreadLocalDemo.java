package school.lesson7.data;

public class ThreadLocalDemo {

    public static void main(String[] args) {

        LocalCountingPrinter printer = new LocalCountingPrinter();

        Thread hello = new LocalPrinterThread(printer, "Hello !");
        Thread yo = new LocalPrinterThread(printer, "Yo !");

        hello.start();
        yo.start();

        try {
            hello.join();
            yo.join();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class LocalCountingPrinter {

    private ThreadLocal<Integer> counter = new ThreadLocal<Integer>();

    public void print(String text) {

        System.out.println("Thread: " + Thread.currentThread().getName()
                + " printing: " + text);

        if (counter.get() == null) {
            counter.set(1);
        } else {
            counter.set(counter.get() + 1);
        }
    }

    public int getCount() {
        return counter.get();
    }
}

class LocalPrinterThread extends Thread {

    private LocalCountingPrinter printer;
    private String greeting;

    public LocalPrinterThread(LocalCountingPrinter printer, String greeting) {
        this.printer = printer;
        this.greeting = greeting;
    }

    public void run() {

        for (int i = 0; i < Math.random() * 10; i++) {
            printer.print(greeting);
        }
        System.out.println("Thread: " + getName() + " printed: "
                + printer.getCount() + " greetings");
    }
}
