package school.lesson7.data;

public class RegularCounterDemo {

    public static void main(String[] args) {
        
        RegularCounter counter = new RegularCounter();

        Thread one = new IncrementThread(counter);
        Thread two = new IncrementThread(counter);
        
        one.start();
        two.start();
        
        try {
            one.join();
            two.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
       System.out.println(counter.getValue());
    }
}

class RegularCounter {

    private int value;

    public int getValue(){
        return value;
    }

    public int increment(){
        return value++;
    }
}
class IncrementThread extends Thread{

    private RegularCounter counter;
    
    public IncrementThread(RegularCounter counter){
        this.counter = counter;
    }
    
    public void run() {
        for(int i = 0; i < 10000; i++){
            counter.increment();
        }
    }
}
