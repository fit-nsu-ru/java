package school.lesson7.data;

public class SynchronizedCounterDemo {

    public static void main(String[] args) {
        
        SynchronizedCounter counter = new SynchronizedCounter();

        Thread one = new SynchronizedIncrementThread(counter);
        Thread two = new SynchronizedIncrementThread(counter);
        
        one.start();
        two.start();
        
        try {
            one.join();
            two.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
       System.out.println(counter.getValue());
    }
}

class SynchronizedCounter {

    private int value;

    public synchronized int getValue() {
        return value;
    }

    public synchronized int increment() {
        return ++value;
    }
}

class SynchronizedIncrementThread extends Thread{

    private SynchronizedCounter counter;
    
    public SynchronizedIncrementThread(SynchronizedCounter counter){
        this.counter = counter;
    }
    
    public void run() {
        for(int i = 0; i < 10000; i++){
            counter.increment();
        }
    }
}
