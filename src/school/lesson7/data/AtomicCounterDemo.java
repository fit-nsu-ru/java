package school.lesson7.data;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounterDemo {

    public static void main(String[] args) {

        AtomicCounter counter = new AtomicCounter();

        Thread one = new AtomicIncrementThread(counter);
        Thread two = new AtomicIncrementThread(counter);

        one.start();
        two.start();

        try {
            one.join();
            two.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(counter.getValue());
    }
}

class AtomicCounter {
    private AtomicInteger c = new AtomicInteger(0);

    public void increment() {
        c.getAndIncrement();
    }

    public int getValue() {
        return c.get();
    }
}

class AtomicIncrementThread extends Thread {

    private AtomicCounter counter;

    public AtomicIncrementThread(AtomicCounter counter) {
        this.counter = counter;
    }

    public void run() {
        for (int i = 0; i < 10000; i++) {
            counter.increment();
        }
    }
}
