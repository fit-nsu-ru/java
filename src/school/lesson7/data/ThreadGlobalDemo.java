package school.lesson7.data;

public class ThreadGlobalDemo {

    public static void main(String[] args) {

        CountingPrinter printer = new CountingPrinter();

        Thread thread1 = new PrinterThread(printer, "Hello !");
        Thread thread2 = new PrinterThread(printer, "Yo !");

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class CountingPrinter {
    private int counter;

    public void print(String text) {

        System.out.println("Thread: " + Thread.currentThread().getName()
                + " printing: " + text);
        counter++;
    }

    public int getCount() {
        return counter;
    }
}

class PrinterThread extends Thread {

    private CountingPrinter printer;
    private String greeting;

    public PrinterThread(CountingPrinter printer, String greeting) {
        this.printer = printer;
        this.greeting = greeting;
    }

    public void run() {

        for (int i = 0; i < Math.random() * 10; i++) {
            printer.print(greeting);
        }
        System.out.println("Thread: " + getName() + " printed: "
                + printer.getCount() + " greetings");
    }
}
