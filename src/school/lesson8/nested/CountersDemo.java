package school.lesson8.nested;

public class CountersDemo {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = Counters.synchronizedCounter(new SlowCounter());

        Thread t1 = new IncrementThread(counter);
        Thread t2 = new IncrementThread(counter);
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();

        System.out.println("Final value: " + counter.getValue());
    }
}

class IncrementThread extends Thread{

    private Counter counter;
    
    public IncrementThread(Counter counter){
        this.counter = counter;
    }
    
    public void run() {
        for(int i = 0; i < 10000; i++){
            counter.increment();
        }
    }
}

interface Counter {
    
    int getValue();
    int increment();
}

class SlowCounter implements Counter{
    
    private int value;

    public int getValue(){
        return value;
    }

    public int increment(){
        return value++;
    }
}

class Counters {

    public static Counter synchronizedCounter(Counter counter) {
        return new SynchronizedCounter(counter);
    }

    private static class SynchronizedCounter implements Counter {

        private Counter counter;

        public SynchronizedCounter(Counter counter) {
            this.counter = counter;
        }

        public int getValue() {
                return counter.getValue();
        }

        public synchronized int increment() {
                return counter.increment();
        }
    }
}