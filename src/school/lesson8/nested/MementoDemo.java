package school.lesson8.nested;

public class MementoDemo {

    public static void main(String[] args) {
        Speedometer speedo = new Speedometer();

        speedo.setSpeed(50);
        speedo.setSpeed(100);
        System.out.println(speedo);

        Object memento = speedo.saveToMemento();

        speedo.setSpeed(80);
        System.out.println("After setting to 80...");
        System.out.println(speedo);
       
        System.out.println("Now restoring state...");
        speedo.restoreFromMemento(memento);
        System.out.println(speedo);
    }
}

class Speedometer {

    private int speed;
    private int previousSpeed;

    public Speedometer() {
        this.speed = 0;
        this.previousSpeed = 0;
    }

    public void setSpeed(int speed) {
        previousSpeed = this.speed;
        this.speed = speed;
    }

    public String toString() {
        return "Speedometer [speed=" + speed + ", previousSpeed="
                + previousSpeed + "]";
    }

    public Object saveToMemento() {
        System.out.println("Originator: Saving to memento");
        return new SpeedometerMemento(this);
    }

    public void restoreFromMemento(Object o) {
        if (o instanceof SpeedometerMemento) {
            SpeedometerMemento m = (SpeedometerMemento) o;
            speed = m.getCopyOfSpeed();
            previousSpeed = m.getCopyOfPreviousSpeed();
        }
    }

    public static class SpeedometerMemento {

        private int copyOfSpeed;
        private int copyOfPreviousSpeed;

        public SpeedometerMemento(Speedometer s) {
            copyOfSpeed = s.speed;
            copyOfPreviousSpeed = s.previousSpeed;
        }

        public int getCopyOfSpeed() {
            return copyOfSpeed;
        }

        public int getCopyOfPreviousSpeed() {
            return copyOfPreviousSpeed;
        }
    }
}