package school.lesson8.nested;

class Professor {
      
    private enum Mode {

        WRITE_MODE {
            public void work() { System.out.println("Writing..."); }
            public void drink() { System.out.println("Drinking coffee..."); }
        },
        TEACH_MODE {
            public void work() { System.out.println("Teaching..."); }
            public void drink() { System.out.println("Drinking water..."); }
        },
        ADM_MODE { 
            public void work() { System.out.println("Doing administrative work..."); }
            public void drink() { System.out.println("Drinking tea..."); }
        };

        abstract void work();
        abstract void drink();
    }

    private Mode mode;

    public Professor() {
        mode = Mode.WRITE_MODE; 
    }
    
    public void doing() {
        mode.work();
    }

    public void drink() {
        mode.drink();
    }

    public void setWrite() {
        mode = Mode.WRITE_MODE;
    }

    public void setTeach() {
        mode = Mode.TEACH_MODE;
    }

    public void setAdministrate() {
        mode = Mode.ADM_MODE;
    }
}

public class StateEnumDemo {
    
    public static void main(String[] args) {
        Professor professor = new Professor();
        professor.doing();
        professor.drink();
        professor.setTeach();
        professor.doing();
        professor.drink();
    }
}