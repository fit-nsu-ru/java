package school.lesson8.nested;

class TestDemo {

    private String name;

    private TestDemo(String name) {
        this.name = name;
    }

    private static void sayHello(String name) {
        System.out.println("Hello " + name + "!");
    }

    private void sayHello() {
        System.out.println("Hello " + name + "!");
    }
        
    private static class InnerTest {
        public static void main(String[] args) {
            sayHello("Harry Hacker");
            TestDemo hello = new TestDemo("Tonny Tester");
            hello.sayHello();
        }
    }
}