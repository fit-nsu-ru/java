package school.lesson8.nested;

import java.util.NoSuchElementException;

public class LinkedListDemo {

    public static void main(String[] args) {

        String[] arguments = { "orange", "kiwi", "banana", "lemon", "apple",
                "pear", "grapes", "mango" };

        StringLinkedList fruits = new StringLinkedList();

        for (String f : arguments)
            fruits.add(f);

        System.out.println("List contents: " + fruits);
        System.out.println("List size: " + fruits.getSize());
    }
}

class StringLinkedList {

    private int size = 0;
    private Entry header = new Entry(null, null, null);

    public StringLinkedList() {
        header.next = header.previous = header;
    }

    private static class Entry {

        public Entry(String data, Entry next, Entry previous) {
            this.data = data;
            this.next = next;
            this.previous = previous;
        }

        String data;
        Entry next;
        Entry previous;
    }

    public void add(String s) {

        Entry newEntry = new Entry(s, header, header.previous);
        newEntry.previous.next = newEntry;
        newEntry.next.previous = newEntry;
        size++;
    }

    public boolean remove(String s) {
        if (s == null) {
            for (Entry e = header.next; e != header; e = e.next) {
                if (e.data == null) {
                    remove(e);
                    return true;
                }
            }
        } else {
            for (Entry e = header.next; e != header; e = e.next) {
                if (e.equals(e.data)) {
                    remove(e);
                    return true;
                }
            }
        }
        return false;
    }

    private void remove(Entry e) {
        if (e == header)
            throw new NoSuchElementException();

        e.previous.next = e.next;
        e.next.previous = e.previous;
        e.next = e.previous = null;
        e.data = null;
        size--;
    }

    public String toString() {

        StringBuilder b = new StringBuilder();
        for (Entry e = header.next; e != header; e = e.next) {
            b.append(e.data);
            b.append(" ");
        }
        return b.toString();
    }
    
    public int getSize() {
        return size;
    }
    
}