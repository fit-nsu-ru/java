package school.lesson8.inner;

public class MementoDemo {

    public static void main(String[] args) {
        Speedometer speedo = new Speedometer();

        speedo.setSpeed(50);
        speedo.setSpeed(100);
        System.out.println(speedo);

        Speedometer.SpeedometerMemento memento = speedo.createSpeedometerMemento();

        speedo.setSpeed(80);
        System.out.println("After setting to 80...");
        System.out.println(speedo);

        System.out.println("Now restoring state...");
        memento.restoreState();
        System.out.println(speedo);
    }
}

class Speedometer {

    private int speed;
    private int previousSpeed;

    public Speedometer() {
        this.speed = 0;
        this.previousSpeed = 0;
    }

    public void setSpeed(int speed) {
        previousSpeed = this.speed;
        this.speed = speed;
    }

    public String toString() {
        return "Speedometer [speed=" + speed + ", previousSpeed="
                + previousSpeed + "]";
    }
    
    
    public SpeedometerMemento createSpeedometerMemento() {
        
        SpeedometerMemento memento = new SpeedometerMemento();
        return memento;      
    }
 
    class SpeedometerMemento {

        private int copyOfSpeed;
        private int copyOfPreviousSpeed;

        public SpeedometerMemento() {
            copyOfSpeed = speed;
            copyOfPreviousSpeed = previousSpeed;
        }
        
        public void setState() {
            copyOfSpeed = speed; 
            copyOfPreviousSpeed = previousSpeed; 
       } 
      
        public void restoreState() {
            speed = copyOfSpeed;
            previousSpeed = copyOfPreviousSpeed;
       }    
    }
}