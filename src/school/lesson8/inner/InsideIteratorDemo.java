package school.lesson8.inner;


class IntList {
    
    private final static int SIZE = 15;
    private int[] arrayOfInts = new int[SIZE];
    
    public IntList() {
 
        for (int i = 0; i < SIZE; i++) {
            arrayOfInts[i] = i;
        }
    }
    
    public void printEven() {
        InnerEvenIterator iterator = this.new InnerEvenIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.getNext() + " ");
        }
    }
    
    private class InnerEvenIterator {
        private int next = 0;
        
        public boolean hasNext() {
            return (next <= SIZE - 1);
        }
        
        public int getNext() {
            int retValue = arrayOfInts[next];
            next += 2;
            return retValue;
        }
    }
}

public class InsideIteratorDemo {
    
    public static void main(String s[]) {
        IntList list = new IntList();
        list.printEven();
    }
}