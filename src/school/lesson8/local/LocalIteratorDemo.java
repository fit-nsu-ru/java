package school.lesson8.local;

import java.util.Iterator;

public class LocalIteratorDemo {

    public static void main(String[] args) {

        LocalStringList list = new LocalStringList();
        list.add("Harry Hacker");
        list.add("Tony Tester");
        list.add("Cindy Coder");

        Iterator<String> iter = list.iterator();
        
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}

class LocalStringList implements Iterable<String> {

    private String[] strings = new String[100];
    private int size;

    public void add(String string) {
        strings[size++] = string;
    }

    public Iterator<String> iterator() {
  
        class LocalIterator implements Iterator<String> {

            private int position = 0;

            public String next() {
                return strings[position++];
            }

            public boolean hasNext() {
                return position < size;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
        return new LocalIterator();
    }
}