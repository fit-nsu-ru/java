package school.lesson8.local;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AnonymousComparator {
    public static void main(String args[]) {

        List<String> fruits = new ArrayList<String>();

        fruits.add("Orange");
        fruits.add("Tomato");
        fruits.add("Onion");
        fruits.add("Apple");
        fruits.add("Cucumber");
        fruits.add("Potato");

        System.out.println(fruits);
       
        System.out.println("Sorting in reverse order...");
        
        Collections.sort(fruits, new Comparator<String>() {
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });

        System.out.println(fruits);
    }
}