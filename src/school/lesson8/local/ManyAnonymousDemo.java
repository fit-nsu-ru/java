package school.lesson8.local;

public class ManyAnonymousDemo {

    interface Greeter {
        public void greet(String name);
    }
 
    public static void main(String[] args) {

        Greeter firstGreeter = new Greeter() {

            public void greet(String name) {
                System.out.println("Hello " + name);
            }
        };

        Greeter secondGreeter = new Greeter() {

            public void greet(String name) {
                System.out.println("Hello " + name);
            }
        };
 
        firstGreeter.greet("Harry Hacker");
        secondGreeter.greet("Tony Tester");
    }
}
