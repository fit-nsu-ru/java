package school.lesson8.local;

import java.util.Comparator;

public class BaseConstructorDemo {

    static abstract class Greeter {
        
        private String greeting;
        
        public Greeter(String greeting) { 
            this.greeting = greeting;
        }
        
        public void greet(String name) {
            System.out.println(greeting + " " + name);
        }
    }

    public static void main(String[] args) {

        Greeter doubleEnglishGreeter = new Greeter("Hello") {

            public void greet(String name) {
                super.greet(name);
                super.greet(name);
            }
        };
        
        Greeter tripleFrenchGreeter = new Greeter("Salut") {

            public void greet(String name) {
                super.greet(name);
                super.greet(name);
                super.greet(name);
            }
        };

        doubleEnglishGreeter.greet("Harry Hacker");
        tripleFrenchGreeter.greet("Tonny Tester");
    }
}
