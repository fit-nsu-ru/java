package school.lesson8.local;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LocalComparatorDemo {
    public static void main(String args[]) {

        List<String> fruits = new ArrayList<String>();

        fruits.add("Orange");
        fruits.add("Grapes");
        fruits.add("Banana");
        fruits.add("Apple");
        fruits.add("Watermelon");
        fruits.add("Kiwi");
        fruits.add("Melon");

        System.out.println(fruits);

        System.out.println("Sorting in reverse order...");
     
        class ReverseComparator implements Comparator<String> {
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        }

        Collections.sort(fruits, new ReverseComparator());
        System.out.println(fruits);
    }
}
