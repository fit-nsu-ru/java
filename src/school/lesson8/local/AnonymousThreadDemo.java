package school.lesson8.local;

import java.util.Date;

public class AnonymousThreadDemo {

    public static void main(String[] args) {

        Thread t = new Thread() {
            public void run() {
                for (int i = 0; i < 10; i++) {
                    Date now = new Date();
                    System.out.println("Hello World! " + now);
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }
}
