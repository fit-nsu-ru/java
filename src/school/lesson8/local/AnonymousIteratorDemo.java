package school.lesson8.local;

import java.util.Iterator;

public class AnonymousIteratorDemo {

    public static void main(String[] args) {

        AnonymousStringList list = new AnonymousStringList();
        list.add("Harry Hacker");
        list.add("Tony Tester");
        list.add("Cindy Coder");

        Iterator<String> iter = list.iterator();
        
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}

class AnonymousStringList implements Iterable<String> {

    private String[] strings = new String[100];
    private int size;

    public void add(String string) {
        strings[size++] = string;
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {

            private int position = 0;

            public String next() {
                return strings[position++];
            }

            public boolean hasNext() {
                return position < size;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
