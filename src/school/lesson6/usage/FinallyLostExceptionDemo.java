package school.lesson6.usage;

public class FinallyLostExceptionDemo {

    public static void main(String[] args) {
        try {
            throw new RuntimeException();
        } finally {
            System.out.println("Return in finally block will");
            System.out.println("make any exception disappear");

            return;
        }
    }
}