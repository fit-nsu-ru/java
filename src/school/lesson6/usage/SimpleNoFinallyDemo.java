package school.lesson6.usage;

public class SimpleNoFinallyDemo {

    public static void main(String[] args) {

        String[] numbers = {"7", "2", "4", "Hello", "3", "World", "5"};
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            try {
                int number = Integer.parseInt(numbers[i]);
                sum += number;
                System.out.println("i = " + i);
            } catch (NumberFormatException nfe) {
                System.out.print("Not a number! ");
                System.out.println("i = " + i);
            } 
        }
        System.out.println("Sum: " + sum);
    }
}
