package school.lesson6.usage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FinallyReaderDemo {

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(
                System.in));

        System.out.println("Please enter the file name: ");
        String filename = console.readLine();

        BufferedReader file = null;
            
        try {
            int total = 0;
            String line;
            file = new BufferedReader(new FileReader(filename));

            while ((line = file.readLine()) != null) {
                total += Integer.parseInt(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (file != null)
                    file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}