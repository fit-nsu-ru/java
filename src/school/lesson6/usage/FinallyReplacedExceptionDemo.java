package usage;

class VeryImportantException extends Exception {
    public String toString() {
        return "A very important exception!";
    }
}

class NotSoImportantException extends Exception {
    public String toString() {
        return "Not so important exception";
    }
}

class SomeClass {

    void someMethod() throws VeryImportantException {
        throw new VeryImportantException();
    }

    void close() throws NotSoImportantException {
        throw new NotSoImportantException();
    }
}

public class FinallyReplacedExceptionDemo {

    public static void main(String[] args) {
        try {
            SomeClass some = new SomeClass();
            try {
                some.someMethod();
            } finally {
                some.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}


