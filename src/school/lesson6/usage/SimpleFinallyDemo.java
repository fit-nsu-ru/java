package school.lesson6.usage;

public class SimpleFinallyDemo {

    public static void main(String[] args) {

        String[] numbers = {"7", "2", "4", "Hello", "3", "World", "5"};
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            try {
                int number = Integer.parseInt(numbers[i]);
                sum += number;
                
            } catch (NumberFormatException e) {
                System.out.print("Not a number! ");
            } finally {
                System.out.println("i = " + i);
            }
        }
        System.out.println("Sum: " + sum);
    }
}
