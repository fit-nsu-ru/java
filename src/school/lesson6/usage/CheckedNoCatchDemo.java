package school.lesson6.usage;


import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class CheckedNoCatchDemo {

   public static void main(String[] args) throws FileNotFoundException {

      Scanner consoleIn = new Scanner(System.in);
      System.out.println("Enter file name: ");
      String fileName = consoleIn.nextLine();

      Scanner fileIn = new Scanner(new File(fileName));

      int count = 0;
      while (fileIn.hasNext()) {
         String word = fileIn.next();
         count++;
      }
      System.out.println("Number of words is " + count);
      fileIn.close();
   }
}
