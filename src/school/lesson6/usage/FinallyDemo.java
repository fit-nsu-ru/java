package school.lesson6.usage;

class FinallyDemo {
    
    static void methodA() {
      try {
        System.out.println("Enter methodA()");
        throw new RuntimeException("demo");
      } finally {
        System.out.println("methodA's finally");
      }
    }
    
    static void methodB() {
      try {
        System.out.println("Enter methodB");
        return;
      } finally {
        System.out.println("methodB's finally");
      }
    }
   
    static void methodC() {
      try {
        System.out.println("Enter methodC");
      } finally {
        System.out.println("methodC's finally");
      }
    }

    public static void main(String args[]) {
      try {
        methodA();
      } catch (Exception e) {
        System.out.println("Exception from methodA caught");
      }
      methodB();
      methodC();
    }
  }

