package school.lesson6.usage;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class CheckedDemo {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner consoleIn = new Scanner(System.in);
        File file = null;

        do {
            System.out.println("Enter file name: ");
            String fileName = consoleIn.nextLine();
            file = new File(fileName);

        } while (!file.exists());

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Scanner fileIn = new Scanner(file);

        int count = 0;
        while (fileIn.hasNext()) {
            String word = fileIn.next();
            count++;
        }
        System.out.println("Number of words is " + count);
        fileIn.close();
    }
}
