package usage;

class Calculator {

    int someMethod(int i) {
        try {
            return i*1000;
        } finally {
            return 100;
        }
    }
}

public class FinallyReplacedReturn {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        for (int i = 0; i < 10; i++) {
            System.out.println(calculator.someMethod(i));
        }
    }
}
