package school.lesson6.usage;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UncheckedMultipleCatchDemo {

   public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      while (true) {

         try {

            System.out.println("\nEnter first number: ");
            int n1 = input.nextInt();
            
            System.out.println("Enter second number: ");
            int n2 = input.nextInt();
            
            int result = n1 / n2;
            System.out.println("The result is: " + result);
         }

         catch (InputMismatchException e) {
            System.out.println("Input must be a number!");
            input.next();
         }
         catch (ArithmeticException e) {
            System.out.println("Second number must be non-zero!");
         }
      }
   }
}