package school.lesson6.usage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class NestedTryDemo {

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(
                System.in));

        while (true) {
            try {

                System.out.println("Please enter a file name: ");
                String name = console.readLine();

                BufferedReader file = new BufferedReader(new FileReader(name));

                FileWriter fw = null;
                String line = null;

                while ((name = file.readLine()) != null) {

                    try {
                        fw = new FileWriter(name);
                        fw.write("Hello world!");
                        System.out.println("   Processes file " + name);
                    } catch (IOException e) {
                        System.out
                                .println("Error processing file from the list");
                    } finally {

                        if (fw != null) {

                            try {
                                fw.close();
                            } catch (IOException e) {
                            }
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                System.out.println("File list file not found");
            }
        }
    }
}
