package school.lesson6.usage;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UncheckedCatchDemo {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        while (true) {

            try {

                System.out
                        .println("\nInteger division application.\nEnter first number: ");
                int n1 = input.nextInt();

                System.out.println("Enter second number: ");
                int n2 = input.nextInt();

                int result = n1 / n2;
                System.out.println("The result is: " + result);
            }

            catch (RuntimeException e) {
                System.out
                        .println("Input must be a number, the second number must be non-zero!");
            }
        }
    }
}