package school.lesson6.usage;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class CheckedCatchDemo {

    public static void main(String[] args) {

        Scanner fileIn = null;

        do {
            Scanner consoleIn = new Scanner(System.in);
            System.out.println("Enter a file name: ");
            String fileName = consoleIn.nextLine();
            try {
                fileIn = new Scanner(new File(fileName));
            } catch (FileNotFoundException ex) {
                System.out.println("Error: File not found");
            }
        } while (fileIn == null);

        int count = 0;
        while (fileIn.hasNext()) {
            String word = fileIn.next();
            count++;
        }
        System.out.println("Number of words is " + count);
        fileIn.close();
    }
}
