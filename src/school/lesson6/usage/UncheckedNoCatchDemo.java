package school.lesson6.usage;

import java.util.Scanner;

public class UncheckedNoCatchDemo {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter first number: ");
        int n1 = input.nextInt();

        System.out.println("Enter second number: ");
        int n2 = input.nextInt();

        int result = n1 / n2;
        System.out.println("The result is: " + result);
    }
}
