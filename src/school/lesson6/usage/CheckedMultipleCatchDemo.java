package school.lesson6.usage;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class CheckedMultipleCatchDemo {

    public static void main(String[] args) {

        BufferedReader console = new BufferedReader(new InputStreamReader(
                System.in));

        while (true) {

            try {

                System.out.println("Please enter the file name: ");
                String filename = console.readLine();

                FileWriter out = new FileWriter(new File(filename));
                out.write("Hello World!");
                out.close();

            } catch (FileNotFoundException e) {
                System.out.println("File not found");

            } catch (EOFException e) {
                System.out.println("End of file reached");

            } catch (IOException e) {
                System.out.println("General I/O exception");
            }
        }
    }
}