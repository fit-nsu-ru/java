package school.lesson6.usage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AdvancedFinallyDemo {

    public static void main(String args[]) throws IOException {

        BufferedReader reader = null;
        BufferedWriter writer = null;

        String fromFile = "I:\\FileIO\\fromFile.txt";
        String toFile = "I:\\FileIO\\toFile.txt";

        try {

            reader = new BufferedReader(new FileReader(fromFile));
            writer = new BufferedWriter(new FileWriter(toFile));

            String line = null;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
        } finally {
            closeQuietly(reader, writer);
        }
    }

    public static void closeQuietly(Closeable... closeables) {
        for (Closeable c : closeables) {
            if (c != null) {
                try {
                    c.close();
                } catch (Exception ex) {
                }
            }
        }
    }
}

