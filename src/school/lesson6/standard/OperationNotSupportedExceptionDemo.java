package school.lesson6.standard;

import java.util.Collections;
import java.util.Set;

public class OperationNotSupportedExceptionDemo {
  
   public static void main(String[] args) {

      Set<String> greetings = Collections.singleton("Hello"); 
      
      for(String greeting : greetings){
         System.out.println(greeting);
      }
      
      greetings.add("Hi");
      greetings.add("Good morning");
            
      for(String greeting : greetings){
         System.out.println(greeting);
      }
   }
}
