package school.lesson6.standard;

public class OutOfMemoryErrorDemo {

   public static void main(String[] args) {

      int[][] bigArray = new int[10000][];

      for (int i = 0; i < bigArray.length; i++) {

         bigArray[i] = new int[1024*1024];
         System.out.println(i + " MB used");
      }
   }
}
