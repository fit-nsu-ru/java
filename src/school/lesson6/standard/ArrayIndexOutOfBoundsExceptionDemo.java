package school.lesson6.standard;

public class ArrayIndexOutOfBoundsExceptionDemo {

   public static void main(String[] args) {

      int[] anArray = { 0, 100, 200, 300, 400, 500, 600, 700, 800, 900 };

           
      for (int i = 0; i <= 10; i++) {

         System.out.println(anArray[i]);
      }

      System.out.println();
      System.out.println("Array length is: " + anArray.length);
   }
}
