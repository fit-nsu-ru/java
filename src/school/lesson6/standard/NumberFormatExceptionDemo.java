package school.lesson6.standard;

public class NumberFormatExceptionDemo {

   public static void main(String[] args) {

      String[] numbers  = {"9", "25", "14", "11", "33", "hello world", "45"};
      
      int total = 0;
      
      for (String number : numbers) {
         total += Integer.parseInt(number);
         System.out.println("Total so far: " + total);
      }
   }
}
