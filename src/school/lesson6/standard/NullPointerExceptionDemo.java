package school.lesson6.standard;

public class NullPointerExceptionDemo {

   public static void main(String[] args) {

      String[] staff = {"Harry Hacker", "Tonny Tester", "Eve Engineer", null, "Carl Cracker"};
      
      for(String name : staff){
         System.out.println("Hello " + name.toUpperCase());
      }
      
      System.out.println();
      System.out.println("Total staff members: " + staff.length);
   }
}
