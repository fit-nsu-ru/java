package school.lesson6.standard;

import java.lang.Math;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

public class IllegalArgumentExceptionDemo {

   public static void main(String[] args) {

      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
      long now = System.currentTimeMillis();

      System.out.println("Current date and time: " + sdf.format(now));

      sdf = new SimpleDateFormat("Hello World: MM/dd/yyyy HH:mm:ss");
      now = System.currentTimeMillis();

      System.out.println("Current date and time: " + sdf.format(now));

   }
}
