package school.lesson6.standard;

import java.util.Arrays;

public class ArrayStoreExceptionDemo {

   public static void main(String[] args) {
     
      Object staff[] = new String[3];
      
      staff[0] = "Harry Hacker";
      staff[1] = "Tonny Tester";
      staff[2] = new Integer(45);
      
      System.out.println(Arrays.toString(staff));
   }
}
