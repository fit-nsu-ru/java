package school.lesson6.standard;

public class StackOverflowErrorDemo {

    static int depth = 0;
    
   public static void RecursiveSayHello() {
      
      System.out.println("Hello World! " + depth++);
           
      RecursiveSayHello();
   }
   
   public static void main(String[] args) {
      RecursiveSayHello();
   }
}
