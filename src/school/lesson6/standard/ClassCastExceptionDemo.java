package school.lesson6.standard;

public class ClassCastExceptionDemo {

   public static void main(String[] args) {
  
      Object obj = new Integer(10);
      System.out.println("Casting Integer to String.....");
      String str =(String) obj;
      System.out.println("This line will never be printed");
   }
}
