package school.lesson6.antipatterns;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CatchAndIgnore {

    public static void main(String[] args) {

        String[] names = { "I:\\FileIO\\lineFile.txt", "I:\\noSuchDir\\noSuchFile" };

        for (String name : names) {
            printFirstLine(name);
        }
    }

    private static void printFirstLine(String fileName) {

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            System.out.print(br.readLine());
        }

        catch (IOException e) {
        }
    }
}
