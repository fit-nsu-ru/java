package school.lesson6.antipatterns;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class DestructiveWrappingDemo {

    public static void main(String[] args) {

        String[] names = { "I:\\FileIO\\lineFile.txt",
                "I:\\noSuchDir\\noSuchFile" };

        for (String name : names) {
            try {
                writeLine(name, "Hello World!");
            } catch (IOException e) {
                //throw new MyException("Hello: something bad has happened ..... ");
                throw new MyException("Hello: something bad has happened ..... ", e);
            }
        }
    }

    private static void writeLine(String fileName, String line)
            throws IOException {

        Writer wr = new FileWriter(fileName);
        wr.write(line);
        wr.flush();
    }
}

class MyException extends RuntimeException {

    public MyException(String message) {
        super(message);
    }
    
    public MyException(String message, Throwable cause) {
        super(message, cause);
    }
}