package school.lesson6.antipatterns;

public class UnsupOpReturnNull {
    
    public static void main(String[] args) {
        
        SingletonStringMap map= new SingletonStringMap("firstName", "Alice");
        System.out.println(map);
        map.put("lastName", "Coder");
        System.out.println(map.get("lastName").trim());
    }
}

class SingletonStringMap {
        
    private final String k;
    private final String v;

        SingletonStringMap(String key, String value) {
            k = key;
            v = value;
        }

        public int size() {return 1;}
        public boolean isEmpty() {return false;}
        public boolean containsKey(Object key) {return eq(key, k);}
        public boolean containsValue(Object value) {return eq(value, v);}
        public String get(String key) {return (eq(key, k) ? v : null);}
        public String put(String key, String value) {return null;}
        //public String put(String key, String value) { throw new UnsupportedOperationException("Put not supported");}
        public String toString() {return "[" + k  + "," + v +"]";}        
        
        private static boolean eq(Object o1, Object o2) {
            return (o1==null ? o2==null : o1.equals(o2));
        }
}