package school.lesson6.antipatterns;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.IOException;

public class LogAndRethrowDemo {

    public static void main(String[] args) throws IOException {

        BasicConfigurator.configure();
        SomeClass someObject = new SomeClass();
        someObject.methodC();
    }
}

class SomeClass {

    private static Logger logger = Logger.getLogger(SomeClass.class);

    public void methodA() throws IOException {
        try {
            throw new IOException();
        } catch (IOException e) {
            logger.error("Error!", e);
            throw e;
        }
    }

    public void methodB() throws IOException {
        try {
            methodA();
        } catch (IOException e) {
            logger.error("Error!", e);
            throw e;
        }
    }
    
    public void methodC() throws IOException {
        try {
            methodB();
        } catch (IOException e) {
            logger.error("Error!", e);
            System.out.println("Handle Exception here.....");
        }
    }
}

/*
    public void methodC() throws IOException {
        try {
            methodB();
        } catch (IOException e) {
            logger.error("Error!", e);
            throw e;
        }
    }
*/
   
