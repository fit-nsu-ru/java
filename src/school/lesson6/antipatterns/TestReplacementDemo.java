package school.lesson6.antipatterns;

//Coding by exception/Expection handling

public class TestReplacementDemo {

   public static void main(String[] args) {

      String[] names = { "Harry Hacker", "Tonny Tester", "Eve Engineer" };

      long now = System.currentTimeMillis();

      for (int i = 0; i < 100000000; i++) {
         if (3 < names.length) {

            System.out.println(names[3]);
         }
      }

      System.out.println("Time using length check: " + (System.currentTimeMillis() - now) + " ms");
      
      now = System.currentTimeMillis();

      for (int i = 0; i < 100000000; i++) {
         try {
            System.out.println(names[3]);
         } catch (ArrayIndexOutOfBoundsException e) {

         }
      }
      System.out.println("Time using exceptions: " + (System.currentTimeMillis() - now) + " ms");
   }
}
