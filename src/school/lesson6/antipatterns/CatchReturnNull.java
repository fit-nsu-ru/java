package school.lesson6.antipatterns;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CatchReturnNull {

    public static void main(String[] args) {
        
        String line = readFirstLine("I:\\noSuchDir\\noSuchFile");
        
        if (line == null) {
            System.out.println("File is empty");
        }
        else {
            System.out.println("First line in file: " + line);
        }
    }

    private static String readFirstLine(String fileName) {
        try {

            BufferedReader br = new BufferedReader(new FileReader(fileName));
            return br.readLine();
        }

        catch (IOException e) {
           return null;
        }
    }
}
