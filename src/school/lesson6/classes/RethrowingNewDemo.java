package classes;

class SomeException extends Exception {
    public SomeException(String s) {
        super(s);
    }
}

class AnotherException extends Exception {
    public AnotherException(String s) {
        super(s);
    }
}

public class RethrowingNewDemo {

    public static void methodA() throws SomeException {
        System.out.println("Throwing an exception from methodA()");
        throw new SomeException("Thrown from methodA()");
    }

    public static void main(String[] args) {
        try {
            try {
                methodA();
            } catch (SomeException e) {
                System.out.println("SomeException caught in inner try");
                e.printStackTrace(System.out);
                throw new AnotherException("Thrown from inner try");
            }
        } catch (AnotherException e) {
            System.out.println("AnotherException caught in outer try");
            e.printStackTrace(System.out);
        }
    }
}