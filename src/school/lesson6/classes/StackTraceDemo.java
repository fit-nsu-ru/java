package school.lesson6.classes;

public class StackTraceDemo {

    static void methodA() {
        try {
            throw new Exception();
        } catch (Exception e) {
            for (StackTraceElement ste : e.getStackTrace())
                System.out.println(ste);//.getMethodName());
        }
    }

    static void methodB() {
        methodA();
    }

    static void methodC() {
        methodB();
    }

    public static void main(String[] args) {
        methodA();
        System.out.println("--------------------------------");
        methodB();
        System.out.println("--------------------------------");
        methodC();
    }
}