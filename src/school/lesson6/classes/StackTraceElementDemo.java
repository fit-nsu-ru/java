package school.lesson6.classes;

public class StackTraceElementDemo {

    public static void main(String[] args) {

        try {
            methodA();
        } catch (Throwable e) {

            e.printStackTrace();
        }
    }

    public static void methodA() throws RuntimeException {

        RuntimeException t = new RuntimeException("This is a new Exception...");
        StackTraceElement[] trace = new StackTraceElement[] { new StackTraceElement(
                "ClassName", "methodName", "fileName", 5) };

        t.setStackTrace(trace);
        throw t;
    }
}