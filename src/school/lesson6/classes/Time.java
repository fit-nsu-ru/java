package school.lesson6.classes;

public class Time {

    private int hour;
    private int minute;
    private int second;
    
    public Time(int hour, int minute, int second) {
        if(hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
           throw new IllegalArgumentException();
        }
        this.hour = hour;
        this.minute = minute;
        this.second = second;
     }

}
