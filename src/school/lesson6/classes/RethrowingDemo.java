package school.lesson6.classes;

public class RethrowingDemo {

    public static void methodA() throws Exception {
        System.out.println("Originating the exception in methodA()");
        throw new Exception("Thrown from methodA()");
    }

    public static void methodB() throws Exception {
        try {
            methodA();
        } catch (Exception e) {
            System.out.println("Inside methodB");
            e.printStackTrace(System.out);
            throw e;
        }
    }

    public static void methodC() throws Exception {
        try {
            methodA();
        } catch (Exception e) {
            System.out.println("Inside methodC");
            e.printStackTrace(System.out);
            throw (Exception) e.fillInStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            methodB();
        } catch (Exception e) {
            System.out.println("Caught in main");
            e.printStackTrace(System.out);
        }
        try {
            methodC();
        } catch (Exception e) {
            System.out.println("Caught in main");
            e.printStackTrace(System.out);
        }
    }
}