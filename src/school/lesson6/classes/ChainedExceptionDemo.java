package school.lesson6.classes;

public class ChainedExceptionDemo {

    static void methodA() {
        NullPointerException e = new NullPointerException("top layer");
        e.initCause(new ArithmeticException("cause"));
        throw e;
    }

    public static void main(String args[]) {
        try {
            methodA();
        } catch (NullPointerException e) {
            System.out.println("Caught: " + e);
            System.out.println("Original cause: " + e.getCause());
            e.getCause().printStackTrace();
        }
    }
}
