package classes;

class WrongDayException extends Exception {
    public WrongDayException() {
    }

    public WrongDayException(String msg) {
        super(msg);
    }
}

public class ThrowExample {
    void doIt() throws WrongDayException {
        int dayOfWeek = (new java.util.Date()).getDay();
        if (dayOfWeek != 2 && dayOfWeek != 4)
            throw new WrongDayException("Tue. or Thur.");
        
        
        System.out.println("Did it");
    }

    public static void main(String[] argv) {
        try {
            (new ThrowExample()).doIt();
        } catch (WrongDayException e) {
            System.out.println("Sorry, can do it only on " + e.getMessage());
        }
    }
}