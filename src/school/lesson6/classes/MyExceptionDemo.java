package classes;

class MyMagicException extends Exception {
    public MyMagicException(String message) {  //constructor
       super(message);
    }
 }

 public class MyExceptionDemo {

     public static void magic(int number) throws MyMagicException {
       if (number == 8) {
          throw (new MyMagicException("You hit the magic number"));
       }
       System.out.println("You did not hit the magic number");  
    }
    
    public static void main(String[] args) {
       try {
          magic(9);   
          magic(8);   
       } catch (MyMagicException ex) {   
          ex.printStackTrace();
       }
    }
 }