package school.lesson9.enums;

enum Suit {
    SPADES, CLUBS, DIAMONDS, HEARTS
}

public class ValueOfDemo {

    public static void main(String[] args) {

        String[] strings = { "SPADES", "CLUBS", "DIAMONDS", "HEARTS" };

        for (String string : strings) {

            Suit suit = Suit.valueOf(string);
            System.out.println("name = " + suit.name() + ", ordinal = "
                    + suit.ordinal());
        }

        System.out.println();
        
        for (String string : strings) {

            Suit suit = Enum.valueOf(Suit.class ,string);
            System.out.println("name = " + suit.name() + ", ordinal = "
                    + suit.ordinal());
        }
    }
}
