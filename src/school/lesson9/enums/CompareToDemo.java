package school.lesson9.enums;

import java.util.Arrays;

enum Month {
    JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
}

public class CompareToDemo {

    public static void main(String[] args) {

        Month[] months = {Month.DEC, Month.NOV, Month.OCT, Month.SEP,
                Month.AUG, Month.JUL, Month.JUN, Month.MAY, Month.APR,
                Month.MAR, Month.FEB, Month.JAN};
 
       System.out.println(Arrays.toString(months));
       
       System.out.println("Sorting using compareTo.....");
       Arrays.sort(months);
       System.out.println(Arrays.toString(months));
    }
}
