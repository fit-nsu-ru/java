package school.lesson9.enums;

enum Operation {

    PLUS("+") { 
        double apply(double x, double y) { return x + y; }
    },
    MINUS("-") {
        double apply(double x, double y) { return x - y; }
    },
    TIMES("*") {
        double apply(double x, double y) { return x * y; }
    },
    DIVIDE("/") { 
        double apply(double x, double y) { return x / y;  }
    };

    abstract double apply(double x, double y);

    private final String symbol;

    private Operation(String symbol) {
        this.symbol = symbol;
    }
 
    
    public String toString() {
        return symbol;
    }
}

public class InheritanceDemo {

    public static void main(String[] args) {
       
        double x = 20.0;
        double y = 5.0;
        
        for (Operation op : Operation.values())
            System.out.printf("%f %s %f = %f%n", x, op, y, op.apply(x, y));
    }
}
