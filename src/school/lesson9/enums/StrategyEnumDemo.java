package school.lesson9.enums;

public class StrategyEnumDemo {

    public static void main(String[] args) {
        
        double weight = 30;
        double distance = 200;
        
        System.out.println("Shipping costs for weight: " + weight + ", over distance: " + distance);
        
        ShippingInfo info = new ShippingInfo(weight, distance);
        
        for(ShippingMethod method : ShippingMethod.values()) {
            info.setShippingMethod(method);
            System.out.println("Shipping method: " + method + ", shipping cost: " + info.getCost());
        }
    }
}

enum ShippingMethod {
    USPS {
        public double getCost(double weight, double distance) {
            return 100.00;
        }
    },
    FED_EX {
        public double getCost(double weight, double distance) {
            return 200.00;
        }
    },
    UPS {
        public double getCost(double weight, double distance) {
            return 300.00;
        }
    },
    DHL {
        public double getCost(double weight, double distance) {
            return 400.00;
        }
    };

    public abstract double getCost(double weight, double distance);
}

class ShippingInfo {

    private double weight;
    private double distance;
    private ShippingMethod method;

    public ShippingInfo(double weight, double distance) {
        this.weight = weight;
        this.distance = distance;
    }

    public void setShippingMethod(ShippingMethod method) {
        this.method = method;
    }

    public double getCost() {
        return method.getCost(weight, distance);
    }
}
