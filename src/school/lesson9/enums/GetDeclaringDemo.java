package school.lesson9.enums;

public class GetDeclaringDemo {

    public static void main(String[] args) {

        for (Operation op : Operation.values()) {

            System.out.println("Class: " + op.getClass()
                    + ", declaring class: " + op.getDeclaringClass());
        }
    }
}