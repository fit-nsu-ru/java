package school.lesson9.enums;

enum Season { WINTER, SPRING, SUMMER, AUTUMN }

public class NameOrdinalDemo {

    public static void main(String[] args) {
        
        Season season = Season.AUTUMN;
        System.out.println("name = " + season.name());
        System.out.println("ordinal = " + season.ordinal());
    }
}
