package school.lesson9.enums;

enum Direction {
    UP, DOWN, LEFT, RIGHT
}

public class ValuesDemo {

    public static void main(String[] args) {

        Direction[] directions = Direction.values();

        for (Direction direction : directions) {

            System.out.println("name = " + direction.name() + ", ordinal = "
                    + direction.ordinal());
        }

        System.out.println("Now directions by ordinal");
        
        for (int i = 0; i < directions.length; i++) {
            System.out.println("ordinal = " + i + ", direction = " + directions[i]);
        }
    }
}
