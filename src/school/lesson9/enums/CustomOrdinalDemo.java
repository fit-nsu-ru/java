package school.lesson9.enums;

enum Coin {
    PENNY(1), NICKLE(5), DIME(10), QUARTER(25);
    private int value;

    private Coin(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
};

public class CustomOrdinalDemo {

    public static void main(String[] args) {

        Coin[] coins = Coin.values();

        for (Coin coin : coins) {

            System.out.println("name = " + coin.name() + ", ordinal = "
                    + coin.ordinal() + ", value = " + coin.getValue());
        }
    }
}
