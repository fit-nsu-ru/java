package school.lesson9.enums;

enum Shape {
    SQUARE, CIRCLE, TRIANGLE
};

public class NullEqualsDemo {

    public static void main(String[] args) {

        Shape shape = null;

        if (shape.equals(Shape.TRIANGLE)) {

            // if (shape == Shape.TRIANGLE) {
            System.out.println("shape is a triangle!");
        } else {
            System.out.println("shape is not a triangle!");
        }
    }
}
