package school.lesson9.enums;


enum Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

public class EnumIsAClassDemo {

    public static void main(String[] args) {
        
        System.out.println(Day.MONDAY.getClass());
        System.out.println(Day.class);
        System.out.println(Day.class.getSuperclass());
    }
}
