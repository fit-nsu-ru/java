package school.lesson9.enums;

enum Color {
    BLACK, WHITE
};

enum ChessPiece {
    PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING
};

public class CompileEqualsDemo {

    public static void main(String[] args) {

        if (Color.BLACK.equals(ChessPiece.BISHOP)) {
            System.out.println("BLACK equals BISHOP!");
        } else {
            System.out.println("BLACK does not equal BISHOP!");
        }
        /*
         * if (Color.BLACK = ChessPiece.BISHOP) {
         * System.out.println("BLACK equals BISHOP!"); } else {
         * System.out.println("BLACK does not equal BISHOP!"); }
         */
    }
}
