package school.lesson9.enums;

public class ValuesByOrdinalDemo {

    public static void main(String[] args) {

        Direction[] directions = Direction.values();

        System.out.println("Directions by ordinal");
        
        for (int i = 0; i < directions.length; i++) {
            System.out.println("ordinal = " + i + ", direction = " + directions[i]);
        }
    }
}
