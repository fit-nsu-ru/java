package school.lesson9.enums;

public class StringEnumPatternDemo {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}

class StringEnumStatus {  
    
    public static final String STATUS_OPEN = "STATUS_OPEN";  
    public static final String STATUS_STARTED = "STATUS_STARTED";  
    public static final String STATUS_INPROGRESS = "STATUS_INPROGRESS";  
    public static final String STATUS_ONHOLD = "STATUS_ONHOLD";  
    public static final String STATUS_COMPLETED = "STATUS_COMPLETED";  
    public static final String STATUS_CLOSED = "STATUS_CLOSED";  
} 