package school.lesson9.enums;

public class TypesafeEnumPatternDemo {

    public static void main(String[] args) {
        

    }
}

class TypeSafeEnumStatus {  
    
    private final int status;  
      
    private TypeSafeEnumStatus(int aStatus){  
        this.status = aStatus;  
    }  
  
    public static final TypeSafeEnumStatus STATUS_OPEN = new TypeSafeEnumStatus(0);  
    public static final TypeSafeEnumStatus STATUS_STARTED = new TypeSafeEnumStatus(1);  
    public static final TypeSafeEnumStatus STATUS_INPROGRESS = new TypeSafeEnumStatus(2);  
    public static final TypeSafeEnumStatus STATUS_ONHOLD = new TypeSafeEnumStatus(3);  
    public static final TypeSafeEnumStatus STATUS_COMPLETED = new TypeSafeEnumStatus(4);  
    public static final TypeSafeEnumStatus STATUS_CLOSED = new TypeSafeEnumStatus(5);  
  
}