package school.lesson9.enums;

import java.util.concurrent.atomic.AtomicInteger;

enum EnumNumberGenerator {

    INSTANCE;

    private AtomicInteger serial = new AtomicInteger(0);

    public int getNextSerial() {
        return serial.incrementAndGet();
    }
}

public class SingletonEnumDemo {

    public static void main(String[] args) {

        EnumNumberGenerator generator = EnumNumberGenerator.INSTANCE;

        for (int i = 0; i < 10; i++) {
            System.out.println("Obtained next serial number: "
                    + generator.getNextSerial());
        }
    }
}