package school.lesson12.biginteger;

import java.math.BigInteger;

public class ConversionDemo {

    public static void main(String[] args) {
        
        BigInteger A =  new BigInteger("10000000000000000",16);
        System.out.println("This number has 0 in long bits: " + A); 
        BigInteger B =  new BigInteger("123456789ABCDEF0",16);
        System.out.println("This number only long bits: " + B); 
        
        BigInteger C =  A.add(B);
        System.out.println("Their sum is: " + C); 
                
        System.out.println("Sum long value is: " + C.longValue());
    }
}
