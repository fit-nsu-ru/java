package school.lesson12.biginteger;

import java.math.BigInteger;

public class ArithmeticExceptionDemo {

    public static void main(String[] args) {

        BigInteger a = BigInteger.valueOf(10);
        BigInteger b = BigInteger.valueOf(0);

        System.out.println("We will try to divide " + a + " by " + b);
        BigInteger c = a.divide(b);
        
        System.out.println("Division was successful, the result is: " + c);
    }
}
