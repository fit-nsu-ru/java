package school.lesson12.biginteger;

import java.math.BigInteger;

public class EqualsDemo {

    public static void main(String[] args) {

        BigInteger i = new BigInteger("00025");
        BigInteger j = BigInteger.valueOf(25);

        System.out.println("Are references i and j equal: " + (i == j));
        System.out.println("Are values represented by i and j equal: "
                + i.equals(j));

        BigInteger k = new BigInteger("00000");
        BigInteger l = BigInteger.valueOf(0);

        System.out.println("Are references k and l equal: " + (i == j));
        System.out.println("Are values represented by k and l equal: "
                + i.equals(j));
    }
}
