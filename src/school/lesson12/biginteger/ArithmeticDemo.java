package school.lesson12.biginteger;

import java.math.BigInteger;

public class ArithmeticDemo {

    public static void main(String[] args) {

        BigInteger a = new BigInteger("2000000000000");
        BigInteger b = new BigInteger("1000000000000");

        System.out.println(a + " + " + b + " = " + a.add(b));
        System.out.println(a + " - " + b + " = " + a.subtract(b));
        System.out.println(a + " * " + b + " = " + a.multiply(b));
        System.out.println(a + " / " + b + " = " + a.divide(b));
    }
}
