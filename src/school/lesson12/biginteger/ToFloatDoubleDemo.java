package school.lesson12.biginteger;

import java.math.BigInteger;

public class ToFloatDoubleDemo {

    public static void main(String[] args) {
        
        BigInteger bigA = new BigInteger("12345678901234567890");
        System.out.println("BigInteger value is: " + bigA);
        
        double doubleA = bigA.doubleValue();
        System.out.println("Converted double value is: " + doubleA);
        
        float floatA = bigA.floatValue();
        System.out.println("Converted float value is: " + floatA);
    }
}
