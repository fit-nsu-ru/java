package school.lesson12.biginteger;

import java.math.BigDecimal;

public class MinMaxDemo {

    public static void main(String[] args) {

        BigDecimal one = new BigDecimal("1111111111111111");
        BigDecimal two = new BigDecimal("2222222222222222");

        System.out.println("Minimum Value among " + one + " and " + two
                + " is " + one.min(two));
        System.out.println("Maximum Value among " + one + " and " + two
                + " is " + one.max(two));
    }
}
