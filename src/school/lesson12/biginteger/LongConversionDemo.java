package school.lesson12.biginteger;

import java.math.BigInteger;

public class LongConversionDemo {

    public static void main(String[] args) {
        
        BigInteger A =  new BigInteger("7FFFFFFFFFFFFFFF",16);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());
        
        A =  new BigInteger("9223372036854775807");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());
          
        A =  new BigInteger("-8000000000000000",16);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());
          
        A =  new BigInteger("-9223372036854775808",10);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());
        
        A =  new BigInteger("18446744073709551616");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());

        A =  new BigInteger("-18446744073709551616");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.longValue());
    }
}
