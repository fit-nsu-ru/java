package school.lesson12.biginteger;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

public class BubbleDemo {

    public static void main(String[] args) {

        // BigInteger factor = new BigInteger("100000000000000000000");
        BigInteger factor = new BigInteger("1000");

        BigInteger[] numbers = new BigInteger[10];
        Random generator = new Random();

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = BigInteger.valueOf(generator.nextInt(10)).multiply(
                    factor);
        }

        System.out.println("Before sorting: " + Arrays.toString(numbers));

        BigInteger temp;

        for (int i = 1; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length - i; j++) {

                if (numbers[j].compareTo(numbers[j + 1]) > 0) {
                    temp = numbers[j + 1];
                    numbers[j + 1] = numbers[j];
                    numbers[j] = temp;
                }
            }
        }

        // Arrays.sort(numbers);
        System.out.println("After sorting:  " + Arrays.toString(numbers));
    }
}
