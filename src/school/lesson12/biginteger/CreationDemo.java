package school.lesson12.biginteger;

import java.math.BigInteger;
import java.util.Arrays;

public class CreationDemo {
    public static void main(String args[]) {

        BigInteger big = new BigInteger("1000000000000");
        System.out.println("BigInteger as String: " + big);

        byte[] bytes = big.toByteArray();
        System.out.println("BigInteger as an array of bytes: "
                + Arrays.toString(bytes));

        BigInteger restored = new BigInteger(bytes);
        System.out.println("BigInteger restored from bytes as String: "
                + restored);
    }
}