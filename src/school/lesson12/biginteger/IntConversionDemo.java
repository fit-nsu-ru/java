package school.lesson12.biginteger;

import java.math.BigInteger;

public class IntConversionDemo {

    public static void main(String[] args) {
        
        BigInteger A =  new BigInteger("7FFFFFFF",16);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());
        
        A =  new BigInteger("2147483647");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());
          
        A =  new BigInteger("-80000000",16);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());
          
        A =  new BigInteger("-2147483648",10);
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());
                
        A =  new BigInteger("4294967296");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());

        A =  new BigInteger("-4294967296");
        System.out.println("BigInteger value is: " + A +  ", long value is: " + A.intValue());
   }
}
