package school.lesson12.biginteger;

import java.math.BigInteger;

public class ImmutableDemo {

    public static void main(String[] args) {

        BigInteger initial = new BigInteger("0");
        
        BigInteger term1 = new BigInteger("100");
        BigInteger term2 = new BigInteger("200");
        BigInteger term3 = new BigInteger("300");
        BigInteger term4 = new BigInteger("400");
        
        BigInteger result = initial.add(term1).add(term2).add(term3).add(term4);
        
        System.out.println("Equal references ? " + (initial == result));
        
        System.out.println("Initial value " + initial);
        System.out.println("Result value  " + result);
    }
}
