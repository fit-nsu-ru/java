package school.lesson12.biginteger;

import java.math.BigInteger;

public class Integer2BigIntegerDemo {

    public static void main(String[] args) {
    
        Integer myInteger = new Integer(1000);
        BigInteger bigInteger = BigInteger.valueOf(myInteger.intValue());
        
        System.out.println("Integer value: " + myInteger);
        System.out.println("BigInteger value: " + bigInteger);
    }
}
