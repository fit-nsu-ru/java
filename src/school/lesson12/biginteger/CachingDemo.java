package school.lesson12.biginteger;

import java.math.BigInteger;

public class CachingDemo {

    public static void main(String[] args) {

        BigInteger i = new BigInteger("16");
        BigInteger j = BigInteger.valueOf(16);
       
        System.out.println("i==j is " + (i == j));
                       
        BigInteger k = BigInteger.valueOf(16);
        BigInteger l = BigInteger.valueOf(16);
       
        System.out.println("k==l is " + (k == l));
        
        
        BigInteger m = BigInteger.valueOf(17);
        BigInteger n = BigInteger.valueOf(17);
       
        System.out.println("m==n is " + (m == n));
    }
}
