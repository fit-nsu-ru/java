package school.lesson12.biginteger;

import java.math.BigInteger;

public class BigIntegerDemo {

    public static void main(String[] args) {
        
        int A = 2147483647;
        int B = 2147483647;
        int C = A * B;

        BigInteger bigA = BigInteger.valueOf(A);
        BigInteger bigB = BigInteger.valueOf(B);
        BigInteger bigC = bigA.multiply(bigB);

        System.out.println("Using int " + A + " * " + B + " = " + C);
        System.out.println("Using BigInteger " + bigA + " * " + bigB + " = " + bigC);
    }
}
