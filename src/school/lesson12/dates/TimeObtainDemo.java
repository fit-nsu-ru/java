package school.lesson12.dates;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TimeObtainDemo {

    public static void main(String[] args) {

        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z"); 
        long current = System.currentTimeMillis();

        System.out.println(formatter.format(new Date(current)));
    }
}
