package school.lesson12.dates;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarSetDemo {

    public static void main(String[] args) {
        
        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String strTime = simpleFormat.format(new Date());
        
        Calendar calendar = new GregorianCalendar();
                
        calendar.set(Calendar.YEAR,1984);
        calendar.set(Calendar.MONTH,5);
        calendar.set(Calendar.DAY_OF_MONTH,17); 
        
        calendar.set(Calendar.HOUR_OF_DAY,23); 
        calendar.set(Calendar.MINUTE,45);
        calendar.set(Calendar.SECOND,58);
        calendar.set(Calendar.MILLISECOND,731);
        
        System.out.println(simpleFormat.format(calendar.getTime()));
    }
}
