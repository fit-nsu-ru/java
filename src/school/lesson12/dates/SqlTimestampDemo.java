package school.lesson12.dates;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SqlTimestampDemo {

    public static void main(String[] args) {
      
        Calendar calendar = new GregorianCalendar();
        
        java.sql.Timestamp timestamp1 = java.sql.Timestamp.valueOf("1984-05-17 23:45:58.731000000");
        
        calendar.set(Calendar.YEAR, 1984);
        calendar.set(Calendar.MONTH, 4);
        calendar.set(Calendar.DAY_OF_MONTH, 17); 
        
        calendar.set(Calendar.HOUR_OF_DAY, 23); 
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 58);
        
        calendar.set(Calendar.MILLISECOND, 731);
       
        java.sql.Timestamp timestamp2 = new java.sql.Timestamp(calendar.getTime().getTime());
  
        java.sql.Timestamp timestamp3 = java.sql.Timestamp.valueOf( 
                calendar.get(Calendar.YEAR) + "-" + 
                (calendar.get(Calendar.MONTH) + 1) + "-" + 
                calendar.get(Calendar.DATE) + " " +
                calendar.get(Calendar.HOUR_OF_DAY) + ":" + 
                calendar.get(Calendar.MINUTE)  + ":" + 
                calendar.get(Calendar.SECOND) + "." + 
                calendar.get(Calendar.MILLISECOND));
                
        System.out.println(timestamp1);
        System.out.println(timestamp2);
        System.out.println(timestamp3);
    }
}


