package school.lesson12.dates;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarAddDemo {

    public static void main(String[] args) {

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.YEAR, 2011);
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        System.out.println("Before adding: " + formatter.format(calendar.getTime()));
      
        calendar.add(Calendar.MONTH, 13);
        
        System.out.println("After adding: " + formatter.format(calendar.getTime()));
    }
}
