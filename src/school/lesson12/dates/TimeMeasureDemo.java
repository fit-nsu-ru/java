package school.lesson12.dates;

public class TimeMeasureDemo {

    private static void doSomething() throws InterruptedException {

        for (int i = 0; i < 5; i++) {

            System.out.println("Doing something...");
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) throws InterruptedException {

        long startTime = System.currentTimeMillis();
       
        doSomething();
       
        long endTime = System.currentTimeMillis();

        long totalTime = endTime - startTime;
        System.out.println("It took " + (totalTime / 1000)
                + " seconds to do something");
    }
}
