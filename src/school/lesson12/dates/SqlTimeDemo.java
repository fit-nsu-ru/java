package school.lesson12.dates;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SqlTimeDemo {

    public static void main(String[] args) {
      
        Calendar calendar = new GregorianCalendar();
        
        java.sql.Time time1 = java.sql.Time.valueOf("23:45:58.");
        
        calendar.set(Calendar.YEAR, 0);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 0); 
        
        calendar.set(Calendar.HOUR_OF_DAY, 23); 
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 58);
        
        calendar.set(Calendar.MILLISECOND, 0);
       
        java.sql.Time time2 = new java.sql.Time( calendar.getTime().getTime() );
  
        java.sql.Time time3 = java.sql.Time.valueOf( 
                calendar.get(Calendar.HOUR_OF_DAY) + ":" + 
                calendar.get(Calendar.MINUTE)  + ":" + 
                calendar.get(Calendar.SECOND));
        
        System.out.println(time1);
        System.out.println(time2);
        System.out.println(time3);
    }
}


