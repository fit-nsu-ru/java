package school.lesson12.dates;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SqlDateDemo {

    public static void main(String[] args) {
      
        Calendar calendar = new GregorianCalendar();

        java.sql.Date date1 = java.sql.Date.valueOf("1984-05-17");
        
        calendar.set(Calendar.YEAR, 1984);
        calendar.set(Calendar.MONTH, 4);
        calendar.set(Calendar.DAY_OF_MONTH, 17); 
        
        calendar.set(Calendar.HOUR_OF_DAY, 0); 
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
       
        java.sql.Date date2 = new java.sql.Date( calendar.getTime().getTime() );
  
        java.sql.Date date3 = java.sql.Date.valueOf( 
                calendar.get(Calendar.YEAR) + "-" + 
                (calendar.get(Calendar.MONTH) + 1) + "-" + 
                calendar.get(Calendar.DATE) );
        
        System.out.println(date1);
        System.out.println(date2);
        System.out.println(date3);
        
        long h = System.currentTimeMillis();
        
        java.sql.Date date = new java.sql.Date(h);
        
        System.out.println(date.getTime());
        System.out.println(h);
    }
}
