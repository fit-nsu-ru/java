package school.lesson12.dates;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateObtainDemo {

    public static void main(String[] args) {

        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z"); 
        Date current = new Date();

        System.out.println(formatter.format(current));
    }
}
