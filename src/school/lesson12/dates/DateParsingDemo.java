package school.lesson12.dates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateParsingDemo {
    public static void main(String args[]) {

        DateFormat formatter;

        formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss zzzz");
        Date date;

        try {
            date = formatter.parse("Sunday, 17 June 1984 23:45:58 GMT+07:00");

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH); 
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY); 
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            System.out.println("year: " + year + ", month: " + month
                    + ", dayOfMonth: " + dayOfMonth + ", hourOfDay: "
                    + dayOfMonth + ", minute: " + minute + ", second: "
                    + second);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
