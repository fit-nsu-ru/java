package school.lesson12.dates;

import java.util.Calendar;

public class CalendarLenientDemo {

    public static void main(String[] args) {
        
        Calendar calendar = Calendar.getInstance();
        System.out.println("Is calendar lenient? " + calendar.isLenient());
        
        calendar.set(2010, 0, 0);
        System.out.println(calendar.getTime());
                 
        calendar.set(2010, Calendar.FEBRUARY, 29);
        System.out.println(calendar.getTime());
    }
}
