package school.lesson12.dates;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarSetPuzzle {

     public static void main(String[] args) {
 
        final Calendar calendar1 = new GregorianCalendar();
        final int year = 2012;
        final int month = Calendar.MARCH;
        final int day = 23;
        calendar1.set(year, month, day);
         
        final int iterationCount = 1000;
        boolean isEqual = false;
        for (int i = 0; i < iterationCount; i++) {
          final Calendar calendar2 = new GregorianCalendar();
          calendar2.set(year, month, day);
          
//          System.out.println(calendar1.get(Calendar.MILLISECOND));
//          System.out.println(calendar2.get(Calendar.MILLISECOND));
          
          System.out.println(calendar1.equals(calendar2));
        }
    }
}
