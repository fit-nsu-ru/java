package school.lesson12.dates;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateFormattingDemo {
    public static void main(String args[]) {

        Format formatter;
        Calendar calendar = new GregorianCalendar();
        
        calendar.set(Calendar.YEAR,1984);
        calendar.set(Calendar.MONTH,5);
        calendar.set(Calendar.DAY_OF_MONTH,17); 
        calendar.set(Calendar.HOUR_OF_DAY,23); 
        calendar.set(Calendar.MINUTE,45);
        calendar.set(Calendar.SECOND,58);
        calendar.set(Calendar.MILLISECOND,731);
        
        Date date = calendar.getTime();
                
        formatter = new SimpleDateFormat("MM/dd/yy");
        System.out.println(formatter.format(date));
        
        formatter = new SimpleDateFormat("dd/MM/yy");
        System.out.println(formatter.format(date));
        
        formatter = new SimpleDateFormat("dd-MMM-yy");
        System.out.println(formatter.format(date));
        
        formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        System.out.println(formatter.format(date));
        
        formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z");
        System.out.println(formatter.format(date));

        formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss zzzz");
        System.out.println(formatter.format(date));
    }
}