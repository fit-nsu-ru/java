package school.lesson12.dates;

public class NanoTimeMeasureDemo {

    private static void doSomething() {

        for (int i = 0; i < 5; i++) {
            
            System.out.println("Doing something...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        long startTime = System.nanoTime();
        doSomething();
        long endTime = System.nanoTime();

        long totalTime = endTime - startTime;
        System.out.println("It took " + (totalTime / 1000000000)
                + " seconds to do something");
    }
}
