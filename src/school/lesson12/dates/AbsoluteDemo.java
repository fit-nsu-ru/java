package school.lesson12.dates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class AbsoluteDemo {

    public static void main(String[] args) {
               
        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z Z"); 
        
        long zero = 0;
        
        formatter.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        System.out.println(formatter.format(zero));
                
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        System.out.println(formatter.format(zero));
   
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Novosibirsk"));
        System.out.println(formatter.format(zero));
    }
}
