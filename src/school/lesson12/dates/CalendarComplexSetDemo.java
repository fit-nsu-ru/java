package school.lesson12.dates;

import java.util.Calendar;

public class CalendarComplexSetDemo {

    public static void main(String[] args) {

        Calendar cal = Calendar.getInstance();
        System.out.println("Current time is: " + cal.getTime());

        cal.set(1995, 5, 25, 04, 15, 20);
        System.out.println("Altered time is: " + cal.getTime());
    }
}
