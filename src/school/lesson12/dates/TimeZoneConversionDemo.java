package school.lesson12.dates;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeZoneConversionDemo {

    public static void main(String[] args) {
      
        TimeZone timeZoneMOW = TimeZone.getTimeZone("Europe/Moscow");
        TimeZone timeZoneLA = TimeZone.getTimeZone("America/Los_Angeles");
      
        Calendar calendar = new GregorianCalendar();

        calendar.setTimeZone(timeZoneMOW);
        
        long timeMOW = calendar.getTimeInMillis();
        System.out.println("time Moscow      = " + timeMOW);
        System.out.println("hour             = " + calendar.get(Calendar.HOUR_OF_DAY));

        calendar.setTimeZone(timeZoneLA);

        long timeLA = calendar.getTimeInMillis();
        System.out.println("time Los Angeles = " + timeLA);
        System.out.println("hour             = " + calendar.get(Calendar.HOUR_OF_DAY));
    }
}
