package school.lesson12.dates;

import java.util.TimeZone;

public class TimeZoneDemo {

    public static void main(String[] args) {
             
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Novosibirsk");
       
        System.out.println(timeZone.getDisplayName());
        System.out.println(timeZone.getID());
        System.out.println(timeZone.getOffset( System.currentTimeMillis())/60/60/1000);
    }
}
