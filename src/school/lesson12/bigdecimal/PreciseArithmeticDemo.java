package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PreciseArithmeticDemo {

    public static void main(String[] args) {

        BigDecimal a = new BigDecimal("0.002");
        BigDecimal b = new BigDecimal("0.001");

        System.out.println(a + " + " + b + " = " + a.add(b));
        System.out.println(a + " - " + b + " = " + a.subtract(b));
        System.out.println(a + " * " + b + " = " + a.multiply(b));
        System.out.println(a + " / " + b + " = " + a.divide(b));
                
        System.out.println();
        
        a = new BigDecimal("100");
        b = new BigDecimal("0.01");
        
        System.out.println(a + " + " + b + " = " + a.add(b));
        System.out.println(a + " - " + b + " = " + a.subtract(b));
        System.out.println(a + " * " + b + " = " + a.multiply(b));
        System.out.println(a + " / " + b + " = " + a.divide(b));
     }
}
