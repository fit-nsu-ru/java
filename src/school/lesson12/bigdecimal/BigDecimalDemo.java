package school.lesson12.bigdecimal;

import java.math.BigDecimal;

public class BigDecimalDemo {

    public static void main(String[] args) {
        
        double A = 2.0;
        double B = 1.1;
        double C = A - B;

        BigDecimal bigA = new BigDecimal("2.0");
        BigDecimal bigB = new BigDecimal("1.1");
        BigDecimal bigC = bigA.subtract(bigB);
        
        System.out.println("Using double " + A + " - " + B + " = " + C);
        System.out.println("Using BigDecimal " + bigA + " - " + bigB + " = " + bigC);
    }
}
