package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class CachingZeroDemo {

    public static void main(String[] args) {

        BigDecimal i = new BigDecimal(BigInteger.ZERO, 15);
        BigDecimal j = BigDecimal.valueOf(0, 15);
                      
        System.out.println("i = " + i + ", j = " + j + ", i==j is " + (i == j));
                       
        BigDecimal k = BigDecimal.valueOf(0, 15);
        BigDecimal l = BigDecimal.valueOf(0, 15);
       
        System.out.println("k = " + k + ", l = " + l + ", k==l is " + (k == l));
        
        BigDecimal m = BigDecimal.valueOf(0, 16);
        BigDecimal n = BigDecimal.valueOf(0, 16);
       
        System.out.println("m = " + m + ", n = " + n + ", m==n is " + (m == n));
    }
}
