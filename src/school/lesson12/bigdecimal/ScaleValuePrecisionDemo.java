package school.lesson12.bigdecimal;

import java.math.BigDecimal;

public class ScaleValuePrecisionDemo {

    public static void main(String[] args) {
      
        BigDecimal big = new BigDecimal("123.456789");
              
        System.out.println("Big decimal: " + big);
        System.out.println("Value: " + big.unscaledValue());
        System.out.println("Scale: " + big.scale());
        System.out.println("Precision: " + big.precision());
    }
}
