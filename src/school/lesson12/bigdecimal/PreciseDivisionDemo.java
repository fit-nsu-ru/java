package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PreciseDivisionDemo {

    public static void main(String[] args) {

        BigDecimal a = new BigDecimal("1");
        BigDecimal b = new BigDecimal("32");
        System.out.println(a + " / " + b + " = " + a.divide(b));
                
        System.out.println();
        
        a = new BigDecimal("1");
        b = new BigDecimal("3");
        
        System.out.println(a + " / " + b + " = " + a.divide(b));
     }
}
