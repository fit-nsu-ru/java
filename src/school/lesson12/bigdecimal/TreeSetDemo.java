package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
    
    public static void main(String[] args) {
    
        Set<BigDecimal> set = new TreeSet<BigDecimal>(); 
        
        set.add(new BigDecimal("2"));
        set.add(new BigDecimal("2.0"));
        set.add(new BigDecimal("2.00"));
        set.add(new BigDecimal("2.000"));
        
        System.out.println("TreeSet size: " + set.size());
        System.out.println("TreeSet contents: " + set);
    }
}
