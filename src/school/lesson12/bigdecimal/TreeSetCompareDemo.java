package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetCompareDemo {
    
    public static void main(String[] args) {
    
        Set<BigDecimal> set = new TreeSet<BigDecimal>(); 
        
        set.add(new BigDecimal("2"));
        set.add(new BigDecimal("3.0"));
        set.add(new BigDecimal("4.00"));
        set.add(new BigDecimal("5.000"));
        set.add(new BigDecimal("8.00"));
        set.add(new BigDecimal("7.0"));
        set.add(new BigDecimal("6.00"));
        set.add(new BigDecimal("-1.0"));
        set.add(new BigDecimal("-2.00"));
        set.add(new BigDecimal("-3.0"));
        set.add(new BigDecimal("0.000"));
        
        System.out.println("TreeSet size: " + set.size());
        System.out.println("TreeSet contents: " + set);
    }
}
