package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SetScaleDemo {

    public static void main(String[] args) {

        BigDecimal a = new BigDecimal("2.10000");
       
        System.out.println(a);
        System.out.println(a.setScale(4));
        System.out.println(a.setScale(3));
        System.out.println(a.setScale(2));
        System.out.println(a.setScale(1));
        System.out.println(a.setScale(0));       
    }
}
