package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class CachingDemo {

    public static void main(String[] args) {

        BigDecimal i = new BigDecimal(10);
        BigDecimal j = BigDecimal.valueOf(10);

        System.out.println("i = " + i + ", j = " + j + ", i==j is " + (i == j));

        BigDecimal k = BigDecimal.valueOf(10);
        BigDecimal l = BigDecimal.valueOf(10);

        System.out.println("k = " + k + ", l = " + l + ", k==l is " + (k == l));

        BigDecimal m = BigDecimal.valueOf(11);
        BigDecimal n = BigDecimal.valueOf(11);

        System.out.println("m = " + m + ", n = " + n + ", m==n is " + (m == n));
    }
}
