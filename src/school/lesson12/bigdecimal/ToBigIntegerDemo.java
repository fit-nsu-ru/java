package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ToBigIntegerDemo {

    public static void main(String[] args) {
        
        BigDecimal decA =  new BigDecimal("123.456789");
        System.out.println("BigDecimal value is: " + decA);
        
        BigInteger intA = decA.toBigInteger();
        System.out.println("Converted BigInteger value is: " + intA);
        
        intA = decA.toBigIntegerExact();
        System.out.println("Converted BigInteger using exact conversion  value is: " + intA);
    }
}
