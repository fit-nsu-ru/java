package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ContextArithmeticDemo {

    public static void main(String[] args) {

        BigDecimal a = new BigDecimal("2.00000");
        BigDecimal b = new BigDecimal("3.0");
        
        for (int i = 1; i < 10; i++) {
            MathContext context = new MathContext(i, RoundingMode.HALF_UP);
            System.out.println(a + " / " + b + " = " + a.divide(b, context));
        }
  
        for (int i = 1; i < 10; i++) {
            MathContext context = new MathContext(i, RoundingMode.UNNECESSARY);
            System.out.println(a + " / " + b + " = " + a.divide(b, context));
        }
    }
}
