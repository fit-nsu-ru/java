package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ToIntDemo {

    public static void main(String[] args) {

        BigDecimal decA = new BigDecimal("1234567890123456789.0123456789");
        System.out.println("BigDecimal value is: " + decA);

        long longA = decA.longValue();
        System.out.println("Converted long value is: " + longA);

        longA = decA.longValueExact();
        System.out.println("Converted long value using exact conversion is: " + longA);
    }
}
