package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ToFloatDoubleDemo {

    public static void main(String[] args) {
        
        BigDecimal decA = new BigDecimal("1.234567890123456789");
        System.out.println("BigDecimal value is: " + decA);
        
        double doubleA = decA.doubleValue();
        System.out.println("Converted double value is: " + doubleA);
        
        float floatA = decA.floatValue();
        System.out.println("Converted float value is: " + floatA);
    }
}
