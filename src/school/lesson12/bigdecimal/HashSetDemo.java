package school.lesson12.bigdecimal;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {
    
    public static void main(String[] args) {
    
        Set<BigDecimal> set = new HashSet<BigDecimal>(); 
        
        set.add(new BigDecimal("2"));
        set.add(new BigDecimal("2.0"));
        set.add(new BigDecimal("2.00"));
        set.add(new BigDecimal("2.000"));
        
        System.out.println("HashSet size: " + set.size());
        System.out.println("HashSet contents: " + set);
    }
}
