package school.lesson12.bigdecimal;

import java.math.BigDecimal;

public class YesStringConstructorDemo {

    public static void main(String[] args) {
        
           System.out.println(new BigDecimal("0.5"));
           System.out.println(new BigDecimal("0.25"));
           System.out.println(new BigDecimal("0.125"));
           System.out.println(new BigDecimal("0.0625"));
           System.out.println(new BigDecimal("0.03125"));
           
           System.out.println(new BigDecimal("0.1"));
           System.out.println(new BigDecimal("0.01"));
           System.out.println(new BigDecimal("0.001"));
           System.out.println(new BigDecimal("0.0001"));
           System.out.println(new BigDecimal("0.00001"));
    }
}
