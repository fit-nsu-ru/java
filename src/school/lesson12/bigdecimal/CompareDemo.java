package school.lesson12.bigdecimal;

import java.math.BigDecimal;

public class CompareDemo {

    public static void main(String[] args) {
      
        BigDecimal bigA = new BigDecimal("2.00");
        BigDecimal bigB = new BigDecimal("2.0");
        
        System.out.println("Big decimal A: " + bigA + " unscaled value: " + bigA.unscaledValue() + 
                " scale: " + bigA.scale() + " precision: " + bigA.precision());
             
        System.out.println("Big decimal B: " + bigB + " unscaled value: " + bigB.unscaledValue() + 
                " scale: " + bigB.scale() + " precision: " + bigB.precision());
        
        System.out.println("Are A and B equal ? " + (bigA.compareTo(bigB) == 0));
    }
}
